package com.buba.yka.core;

import com.buba.yka.base.utils.JwtUtils;
import org.junit.Test;

/**
 * @ClassName:JwtTokenTest
 * @Auther: YooAo
 * @Description:
 * @Date: 2023/7/25 20:44
 * @Version: v1.0
 */
public class JwtTokenTest {


    @Test
    public void token(){
//        String token = JwtUtils.createToken(1L, "123");
//        System.out.println(token);
        Long userId = JwtUtils.getUserId("eyJhbGciOiJIUzUxMiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAKtWKi5NUrJSCg5y0g0Ndg1S0lFKrShQsjI0szQwNjexsDDRUSotTi3yTAGKQZh-ibmpQB2GRsZKtQCOF-xCPwAAAA.buhMOPEZM08hATSggNLQ2UAzbiskmuR7DHMq5EwsGomQZfVvuqvZm2QH4RTMyw2h1lZPoWUfd5s7xX4cuRunhw");
        String userName = JwtUtils.getUserName("eyJhbGciOiJIUzUxMiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAKtWKi5NUrJSCg5y0g0Ndg1S0lFKrShQsjI0szQwNjexsDDRUSotTi3yTAGKQZh-ibmpQB2GRsZKtQCOF-xCPwAAAA.buhMOPEZM08hATSggNLQ2UAzbiskmuR7DHMq5EwsGomQZfVvuqvZm2QH4RTMyw2h1lZPoWUfd5s7xX4cuRunhw");
        System.out.println(userId);
        System.out.println(userName);
    }

}
