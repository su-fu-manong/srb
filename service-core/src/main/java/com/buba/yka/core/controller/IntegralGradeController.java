package com.buba.yka.core.controller;


import com.buba.yka.core.pojo.entity.IntegralGrade;
import com.buba.yka.core.service.IntegralGradeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 积分等级表 前端控制器，前端用户界面的接口
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
@Api(tags = "网站积分等级接口")
@RestController
@RequestMapping("/api/core/integralGrade")
public class IntegralGradeController {
    @Resource
    private IntegralGradeService integralGradeService;

    /**
     * 积分等级列表接口
     * @return
     */
    @GetMapping("/list")
    @ApiOperation("积分等级列表接口")
    public List<IntegralGrade> listAll(){
        List<IntegralGrade> list = integralGradeService.list();
        return list;
    }
}

