package com.buba.yka.oss.service.impl;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.CannedAccessControlList;
import com.buba.yka.oss.service.FileService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.UUID;

/**
 * @ClassName:FileServiceImpl
 * @Auther: YooAo
 * @Description: 文件上传到阿里云
 * @Date: 2023/7/21 15:53
 * @Version: v1.0
 */
@Service
public class FileServiceImpl implements FileService {

    @Value("${aliyun.oss.endpoint}")
    private String endpoint;  //北京域名
    @Value("${aliyun.oss.keyId}")
    private String keyId;   //子账户id
    @Value("${aliyun.oss.keySecret}")
    private String keySecret;   //子账号keySecret
    @Value("${aliyun.oss.bucketName}")
    private String bucketName;  //bucket存储仓库名称


    /**
     *
     * @param inputStream 上传的文件
     * @param module 文件夹
     * @param fileName 文件名称
     * @return
     */
    @Override
    public String upload(InputStream inputStream, String module, String fileName) {
        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(
                endpoint,
                keyId,
                keySecret);
        //判断oss实例是否存在：如果不存在则创建，如果存在则获取
        if(!ossClient.doesBucketExist(bucketName)){
            //创建bucket
            ossClient.createBucket(bucketName);
            //设置oss实例的访问权限：公共读
            ossClient.setBucketAcl(bucketName, CannedAccessControlList.PublicRead);
        }

        //构建日期路径：avatar/2019/02/26/文件名

        //当天日期，格式为：2019/02/26，当作文件夹名称
        String folder = new DateTime().toString("yyyy/MM/dd");

        //文件名生成：uuid.扩展名
        fileName = UUID.randomUUID().toString() + fileName.substring(fileName.lastIndexOf("."));

        //文件根路径，module：前端传过来的模板（仓库一级文件夹），folder：日期文件夹，fileName：上传的文件
        String key = module + "/" + folder + "/" + fileName;

        //文件上传至阿里云，把inputStream这个文件传到bucketName这个仓库下文件路径为key
        ossClient.putObject(bucketName, key, inputStream);

        // 关闭OSSClient。
        ossClient.shutdown();


        //阿里云文件绝对路径
        //上传成功的仓库里面的图片的路径：
        // https://srb-file-yka.oss-cn-beijing.aliyuncs.com/test/2023/07/21/303f503e-6cbe-4be3-a752-bf444e95f2b1.jpg
        //拆分后就是以下这段路径
        // srb-file-yka：bucketName（仓库名称），
        // oss-cn-beijing.aliyuncs.com：endpoint（域名）
        // test/2023/07/21/303f503e-6cbe-4be3-a752-bf444e95f2b1.jpg：key（文件夹即文件）
        return "https://" + bucketName + "." + endpoint + "/" + key;
    }



    /**
     * 根据路径删除文件
     * @param url
     */
    @Override
    public void removeFile(String url) {

        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(
                endpoint,
                keyId,
                keySecret);

        //文件名（服务器上的文件路径）
        //https://srb-file-yka.oss-cn-beijing.aliyuncs.com/test/2023/07/21/303f503e-6cbe-4be3-a752-bf444e95f2b1.jpg
        String host = "https://" +bucketName + "." + endpoint + "/";
        //截取，删除只要：test/2023/07/21/303f503e-6cbe-4be3-a752-bf444e95f2b1.jpg这部分
        String objectName = url.substring(host.length());

        // 删除文件。
        ossClient.deleteObject(bucketName, objectName);

        // 关闭OSSClient。
        ossClient.shutdown();
    }
}
