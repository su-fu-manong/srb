package com.buba.yka.core.service.impl;

import com.buba.yka.core.mapper.IntegralGradeMapper;
import com.buba.yka.core.pojo.entity.IntegralGrade;
import com.buba.yka.core.service.IntegralGradeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 积分等级表 服务实现类
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
@Service
public class IntegralGradeServiceImpl extends ServiceImpl<IntegralGradeMapper, IntegralGrade> implements IntegralGradeService {

}
