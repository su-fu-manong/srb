package com.buba.yka.core.controller.api;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.buba.yka.base.utils.JwtUtils;
import com.buba.yka.common.result.R;
import com.buba.yka.core.hfb.RequestHelper;
import com.buba.yka.core.pojo.entity.Lend;
import com.buba.yka.core.pojo.entity.LendReturn;
import com.buba.yka.core.service.LendReturnService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Api(tags = "还款计划")
@RestController
@RequestMapping("/api/core/lendReturn")
@Slf4j
public class ApiLendReturnController {

    @Resource
    private LendReturnService lendReturnService;


    /**
     * 在网站端当前用户主页面，可查看的还款计划
     * @param request 当前登录用户id
     * @return
     */
    @ApiOperation("获取还款计划列表")
    @GetMapping("/auth/list/{page}/{limit}")
    public R list(
            @ApiParam(value = "当前页码", required = true)
            @PathVariable Long page,

            @ApiParam(value = "每页记录数", required = true)
            @PathVariable Long limit,

            HttpServletRequest request) {

        String token = request.getHeader("token");
        Long userId = JwtUtils.getUserId(token);
        Page<LendReturn> page1 = new Page<>(page,limit);
        Page<LendReturn> list = lendReturnService.selectLendReturn(page1,userId);
        return R.ok().data("list", list);
    }

    /**
     * 在网站端标的获取还款计划列表，只能当前标的借款人能看见
     * @param lendId 标的id
     * @param request 当前登录用户id
     * @return
     */
    @ApiOperation("获取还款计划列表")
    @GetMapping("/auth/list/{lendId}")
    public R list(
            @ApiParam(value = "标的id", required = true)
            @PathVariable Long lendId, HttpServletRequest request) {

        String token = request.getHeader("token");
        Long userId = JwtUtils.getUserId(token);
        List<LendReturn> list = lendReturnService.selectByLendId(lendId,userId);
        return R.ok().data("list", list);
    }

    /**
     * 对选中的这个期进行还款，尚融宝对汇付宝发送请求并携带参数
     * 汇付宝业务：
     * （1）新增user_return表记录和user_item_return表记录（新增的是data的参数）
     * （2）更新user_account表记录，对借款人账户扣除当月应还款的金额，给对应的投资人账户添加自己投资的金额+利息
     * @param lendReturnId 还款计划id，代表着其中一个月的还款记录
     * @param request 当前用户
     * @return
     */
    @ApiOperation("用户还款")
    @PostMapping("/auth/commitReturn/{lendReturnId}")
    public R commitReturn(
            @ApiParam(value = "还款计划id", required = true)
            @PathVariable Long lendReturnId, HttpServletRequest request) {

        String token = request.getHeader("token");
        Long userId = JwtUtils.getUserId(token);
        String formStr = lendReturnService.commitReturn(lendReturnId, userId);
        return R.ok().data("formStr", formStr);
    }

    /**
     * 汇付宝向尚融宝发送异步回调请求，并携带固定参数
     * 尚融宝业务：
     *  (1)更改还款计划表lend_return (status字段：状态 修改为1:已归还) (real_return_time字段：还款时间) (fee字段：手续费，是0，没有手续费，手续费在放款的时候就扣了)
     *  (2)更新标的信息：在最后一次还款后就更新标的状态，status字段修改为3已结清
     *  (3) 更新user_account表记录，对借款人账户扣除当月应还款的金额
     *  (4) 借款人交易流水
     *  (5) 更新回款计划表,更改回款状态（0-未归还 1-已归还）和 回款时间
     *  (6) 更新投资记录表(lend_item)的投资人实际收益(real_amount)。动态增加每个月都会还款都会增加利息
     *  (7) 更新user_account表，投资账号转入金额，给对应的投资人账户添加自己投资的金额+利息
     *  (8) 投资账号交易流水
     * @param request 汇付宝的参数
     * @return
     */
    @ApiOperation("还款异步回调")
    @PostMapping("/notifyUrl")
    public String notifyUrl(HttpServletRequest request) {

        Map<String, Object> paramMap = RequestHelper.switchMap(request.getParameterMap());
        log.info("还款异步回调：" + JSON.toJSONString(paramMap));

        //校验签名
        if(RequestHelper.isSignEquals(paramMap)) {
            if("0001".equals(paramMap.get("resultCode"))) {
                lendReturnService.notify(paramMap);
            } else {
                log.info("还款异步回调失败：" + JSON.toJSONString(paramMap));
                return "fail";
            }
        } else {
            log.info("还款异步回调签名错误：" + JSON.toJSONString(paramMap));
            return "fail";
        }
        return "success";
    }


}