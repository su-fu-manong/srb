package com.buba.yka.sms.openFeign;

import com.buba.yka.sms.openFeign.fallback.CoreUserInfoClientFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @ClassName:UserInfoFegin
 * @Auther: YooAo
 * @Description: 远程调用core模块UserInfoController类
 * @Date: 2023/7/27 10:03
 * @Version: v1.0
 */

//如果远程调用失败了core服务停机了或出现什么问题了，就会走fallback，CoreUserInfoClientFallback这个容错类
@FeignClient(value="service-core",path = "/api/core/userInfo",fallback = CoreUserInfoClientFallback.class)
public interface UserInfoFeign {

    @GetMapping("/checkMobile/{mobile}")
    Boolean checkMobile(@PathVariable("mobile") String mobile);

}
