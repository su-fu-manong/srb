package com.buba.yka.core.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.buba.yka.core.pojo.entity.LendItemReturn;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 标的出借回款记录表 服务类
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
public interface LendItemReturnService extends IService<LendItemReturn> {

    //网站端标的详情中展示回款计划，获取当前标的的所有投资人回款计划
    List<LendItemReturn> selectByLendId(Long lendId, Long userId);

    //通过还款计划的id，找到对应的回款计划数据，组装data参数中的需要的List<Map>集合
    List<Map<String, Object>> addReturnDetail(Long lendReturnId);

    //通过还款计划id获取回款计划列表
    List<LendItemReturn> selectLendItemReturnList(Long lendReturnId);

    //在网站端当前用户主页面，可查看的回款计划
    Page<LendItemReturn> selectLendReturnItem(Page<LendItemReturn> page1, Long userId);
}
