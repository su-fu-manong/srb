package com.buba.yka.oss;

import org.joda.time.DateTime;
import org.junit.Test;

import org.springframework.util.DigestUtils;

import java.util.UUID;

/**
 * @ClassName:Test
 * @Auther: YooAo
 * @Description:
 * @Date: 2023/7/21 15:45
 * @Version: v1.0
 */
public class Test22 {

    @Test
    public void tes33t() {

        String s = UUID.randomUUID().toString();
        System.out.println(s);
        System.out.println(s);

        String s1 = DigestUtils.md5DigestAsHex("12345".getBytes());
        System.out.println(s1);


        //当天日期，格式为：2019/02/26，当作文件夹名称
        String folder = new DateTime().toString("yyyy/MM/dd");

        String folder2 = String.valueOf(new DateTime());

        System.out.println(folder);
        System.out.println(folder2);
    }
}
