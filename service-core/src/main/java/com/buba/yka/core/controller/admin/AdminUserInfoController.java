package com.buba.yka.core.controller.admin;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.buba.yka.base.utils.JwtUtils;
import com.buba.yka.common.exception.Assert;
import com.buba.yka.common.result.R;
import com.buba.yka.common.result.ResponseEnum;
import com.buba.yka.common.utils.RegexValidateUtils;
import com.buba.yka.core.pojo.entity.UserInfo;
import com.buba.yka.core.pojo.query.UserInfoQuery;
import com.buba.yka.core.pojo.vo.LoginVO;
import com.buba.yka.core.pojo.vo.RegisterVO;
import com.buba.yka.core.pojo.vo.UserInfoVO;
import com.buba.yka.core.service.UserInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 后端管理系统，用户基本信息 前端控制器
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
@Api(tags = "会员接口")
@RestController
@RequestMapping("/admin/core/userInfo")
@Slf4j
//@CrossOrigin
public class AdminUserInfoController {

    @Resource
    private UserInfoService userInfoService;

    /**
     * 分页加筛选查询
     * @param page 当前页码
     * @param limit 每页条数
     * @param userInfoQuery 筛选的条件对象表单数据，通过手机号查询或...
     * @return
     */
    @ApiOperation("获取会员分页列表")
    @GetMapping("/list/{page}/{limit}")
    public R listPage(
            @ApiParam(value = "当前页码", required = true)
            @PathVariable Long page,

            @ApiParam(value = "每页记录数", required = true)
            @PathVariable Long limit,

            @ApiParam(value = "查询对象", required = false)
                    UserInfoQuery userInfoQuery) {

        Page<UserInfo> pageParam = new Page<>(page, limit);

        Page<UserInfo> pageModel = userInfoService.listPage(pageParam, userInfoQuery);
        return R.ok().data("pageModel", pageModel);
    }

    /**
     * 更改用户状态status，锁定或解锁，状态（0：锁定 1：正常）
     * @param id
     * @param status
     * @return
     */
    @ApiOperation("锁定和解锁")
    @PutMapping("/lock/{id}/{status}")
    public R lock(
            @ApiParam(value = "用户id", required = true)
            @PathVariable("id") Long id,

            @ApiParam(value = "锁定状态（0：锁定 1：解锁）", required = true)
            @PathVariable("status") Integer status){

        userInfoService.lock(id, status);
        return R.ok().message(status==1?"解锁成功":"锁定成功");
    }

}

