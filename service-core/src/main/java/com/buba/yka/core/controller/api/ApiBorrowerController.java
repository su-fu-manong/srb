package com.buba.yka.core.controller.api;


import com.buba.yka.base.utils.JwtUtils;
import com.buba.yka.common.result.R;
import com.buba.yka.core.pojo.vo.BorrowerVO;
import com.buba.yka.core.service.BorrowerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 借款人 前端控制器
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
@Api(tags = "借款人")
@RestController
@RequestMapping("/api/core/borrower")
@Slf4j
public class ApiBorrowerController {
    @Resource
    private BorrowerService borrowerService;

    /**
     * 提交借款人信息，保存借款人表信息borrower，保存借款人附件表信息borrower_attach
     * @param borrowerVO 组装的表单数据，借款人输入的信息
     * @param request 获取token
     * @return
     */
    @ApiOperation("提交借款人信息，保存借款人信息")
    @PostMapping("/auth/save")
    public R save(@RequestBody BorrowerVO borrowerVO, HttpServletRequest request) {
        String token = request.getHeader("token");
        Long userId = JwtUtils.getUserId(token);//校验token,获取当前登录用户id
        borrowerService.saveBorrowerVOByUserId(borrowerVO, userId);
        return R.ok().message("信息提交成功");
    }


    /**
     * 获取当前登录的账号的借款人信息的审核状态（0：未认证，1：认证中， 2：认证通过， -1：认证失败）
     * @param request 获取token
     * @return 认证状态 （0：未认证，1：认证中， 2：认证通过， -1：认证失败）
     */
    @ApiOperation("获取借款人信息审核状态")
    @GetMapping("/auth/getBorrowerStatus")
    public R getBorrowerStatus(HttpServletRequest request){
        String token = request.getHeader("token");
        Long userId = JwtUtils.getUserId(token);//校验token,获取当前登录用户id

        Integer status = borrowerService.getStatusByUserId(userId);
        return R.ok().data("borrowerStatus", status);
    }



}

