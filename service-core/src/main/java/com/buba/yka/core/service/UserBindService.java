package com.buba.yka.core.service;

import com.buba.yka.core.pojo.entity.UserBind;
import com.baomidou.mybatisplus.extension.service.IService;
import com.buba.yka.core.pojo.vo.UserBindVO;

import java.util.Map;

/**
 * <p>
 * 用户绑定表 服务类
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
public interface UserBindService extends IService<UserBind> {
    /**
     * 账户绑定提交到托管平台的数据
     */
    String commitBindUser(UserBindVO userBindVO, Long userId);

    void notifyUserBind(Map<String, Object> paramMap);

    //通过用户id获取投资人的绑定协议号
    String getBindCodeByUserId(Long investUserId);
}
