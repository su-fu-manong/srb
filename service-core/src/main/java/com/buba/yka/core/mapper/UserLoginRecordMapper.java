package com.buba.yka.core.mapper;

import com.buba.yka.core.pojo.entity.UserLoginRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户登录记录表 Mapper 接口
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
public interface UserLoginRecordMapper extends BaseMapper<UserLoginRecord> {

}
