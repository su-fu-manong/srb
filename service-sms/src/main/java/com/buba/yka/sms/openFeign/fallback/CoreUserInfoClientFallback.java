package com.buba.yka.sms.openFeign.fallback;

import com.buba.yka.sms.openFeign.UserInfoFeign;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 创建容错类
 * 当core服务停机了或出现什么问题了，就停止远程调用了，直接熔断降级，直接走这个方法
 * fallback：当无法校验手机号是否已注册时，直接返回false，代表手机号没有重复，直接发送短信，继续下一步，注册时也做了手机号是否重复的校验
 */
@Service
@Slf4j
public class CoreUserInfoClientFallback implements UserInfoFeign {

    @Override
    public Boolean checkMobile(String mobile) {
        log.error("远程调用失败，服务熔断");
        return false; //代表手机号没有重复
    }
}