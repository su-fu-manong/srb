package com.buba.yka.core.service;

import com.buba.yka.core.pojo.entity.IntegralGrade;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 积分等级表 服务类
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
public interface IntegralGradeService extends IService<IntegralGrade> {

}
