package com.buba.yka.core.controller.admin;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.buba.yka.base.utils.JwtUtils;
import com.buba.yka.common.result.R;
import com.buba.yka.core.pojo.entity.BorrowInfo;
import com.buba.yka.core.pojo.entity.Borrower;
import com.buba.yka.core.pojo.vo.BorrowInfoApprovalVO;
import com.buba.yka.core.service.BorrowInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 借款信息表 前端控制器，后台管理人员对借款申请进行处理
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
@Api(tags = "借款管理")
@RestController
@RequestMapping("/admin/core/borrowInfo")
@Slf4j
public class AdminBorrowInfoController {

    @Resource
    private BorrowInfoService borrowInfoService;


    /**
     * 借款申请信息表（borrow_info）进行列表分页
     * 目的：在实体类中添加姓名，手机号和其它参数（存放数据字典的名称和审核状态）属性
     * 在把returnMethod和moneyUse字段的值通过数据字典显示名称（他们的值是数据字段的value值）展示在后台管理系统页面中
     * @param page
     * @param limit
     * @return
     */
    @ApiOperation("借款申请信息列表分页")
    @GetMapping("/list/{page}/{limit}")
    public R listPage(
            @ApiParam(value = "当前页码", required = true)
            @PathVariable Long page,

            @ApiParam(value = "每页记录数", required = true)
            @PathVariable Long limit,
            @ApiParam(value = "查询关键字", required = false)
            @RequestParam String keyword
    ) {
        Page<BorrowInfo> pageParam = new Page<>(page, limit);

        Page<BorrowInfo> borrowInfoListPage = borrowInfoService.selectListPage(pageParam,keyword);

        return R.ok().data("borrowInfoListPage", borrowInfoListPage);
    }


    /**
     * 通过借款申请id查询借款申请详细信息
     * 需求：借款详情展示借款信息与借款人信息
     * @param id 借款申请id
     * @return 前端展示借款申请信息（borrow_info）和借款人信息（borrower）
     */
    @ApiOperation("获取借款详细信息")
    @GetMapping("/show/{id}")
    public R show(
            @ApiParam(value = "借款id", required = true)
            @PathVariable Long id) {
        Map<String, Object> borrowInfoDetail = borrowInfoService.getBorrowInfoDetail(id);
        return R.ok().data("borrowInfoDetail", borrowInfoDetail);
    }

    /**
     * 借款申请进行审批
     * @param borrowInfoApprovalVO  后端管理系统组装表单：接收借款申请审批表单对象数据
     *  目的：
     *  1、修改借款信息状态
     *  2、审核通过则创建标的
     * 需求：
     * 管理平台借款审批，审批通过后产生标的，审批前我们需要跟借款人进行电话沟通，确定借款年化和平台服务费率（平台收益），
     * 借款年化可能根据实际情况调高或调低；起息日通常我们把它确定为募集结束时间（或放款时间）
     * @return
     */
    @ApiOperation("审批借款申请")
    @PostMapping("/approval")
    public R approval(@RequestBody BorrowInfoApprovalVO borrowInfoApprovalVO) {

        borrowInfoService.approval(borrowInfoApprovalVO);
        return R.ok().message("审批完成");
    }


}

