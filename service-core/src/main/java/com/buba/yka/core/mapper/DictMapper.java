package com.buba.yka.core.mapper;

import com.buba.yka.core.pojo.dto.ExcelDictDTO;
import com.buba.yka.core.pojo.entity.Dict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 数据字典 Mapper 接口
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */

public interface DictMapper extends BaseMapper<Dict> {

    //批量插入
    void insertBatch(List<ExcelDictDTO> list);
}
