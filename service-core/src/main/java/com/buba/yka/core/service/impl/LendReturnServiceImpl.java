package com.buba.yka.core.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.buba.yka.common.exception.Assert;
import com.buba.yka.common.result.ResponseEnum;
import com.buba.yka.core.enums.LendStatusEnum;
import com.buba.yka.core.enums.TransTypeEnum;
import com.buba.yka.core.hfb.FormHelper;
import com.buba.yka.core.hfb.HfbConst;
import com.buba.yka.core.hfb.RequestHelper;
import com.buba.yka.core.mapper.*;
import com.buba.yka.core.pojo.bo.TransFlowBO;
import com.buba.yka.core.pojo.entity.*;
import com.buba.yka.core.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.buba.yka.core.utils.LendNoUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 还款记录表 服务实现类
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
@Service
@Slf4j
public class LendReturnServiceImpl extends ServiceImpl<LendReturnMapper, LendReturn> implements LendReturnService {


    @Resource
    private UserAccountService userAccountService;

    @Resource
    private UserAccountMapper userAccountMapper;

    @Resource
    private LendMapper lendMapper;

    @Resource
    private UserBindService userBindService;

    @Resource
    private LendItemReturnService lendItemReturnService;

    @Resource
    private TransFlowService transFlowService;

    @Resource
    private LendItemReturnMapper lendItemReturnMapper;

    @Resource
    private LendItemMapper lendItemMapper;

    @Resource
    private UserInfoMapper userInfoMapper;

    @Resource
    private DictService dictService;

    /**
     * 管理端标的详情展示还款计划
     * @param lendId 标的id
     * @return
     */
    @Override
    public List<LendReturn> selectByLendId(Long lendId) {
        LambdaQueryWrapper<LendReturn> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(LendReturn::getLendId, lendId);
        List<LendReturn> lendReturnList = baseMapper.selectList(queryWrapper);
        return lendReturnList;
    }

    /**
     * 网站端在标的详情展示还款计划，只能当前标的借款人能看见
     * @param lendId 标的id
     * @param userId 当前登录用户id
     * @return
     */
    @Override
    public List<LendReturn> selectByLendId(Long lendId, Long userId) {
        LambdaQueryWrapper<LendReturn> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(LendReturn::getLendId, lendId).eq(LendReturn::getUserId, userId);
        List<LendReturn> lendReturnList = baseMapper.selectList(queryWrapper);
        return lendReturnList;
    }


    /**
     * 对选中的这个期进行还款，尚融宝对汇付宝发送请求并携带参数
     * 汇付宝业务：
     * （1）新增user_return表记录和user_item_return表记录（新增的是data的参数）
     * （2）更新user_account表记录，对借款人账户扣除当月应还款的金额，给对应的投资人添加自己投资的金额+利息
     * @param lendReturnId 还款计划id，代表着其中一个月的还款记录
     * @param userId 当前用户
     * @return
     */
    @Override
    public String commitReturn(Long lendReturnId, Long userId) {

        //获取选中的这一个月的还款记录
        LendReturn lendReturn = baseMapper.selectById(lendReturnId);

        //判断账号余额是否充足
        BigDecimal amount = userAccountService.getAccount(userId);
        Assert.isTrue(amount.doubleValue() >= lendReturn.getTotal().doubleValue(),
                ResponseEnum.NOT_SUFFICIENT_FUNDS_ERROR);

        //获取借款人code
        String bindCode = userBindService.getBindCodeByUserId(userId);
        //获取lend标的
        Long lendId = lendReturn.getLendId();
        Lend lend = lendMapper.selectById(lendId);

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("agentId", HfbConst.AGENT_ID);//给商户分配的唯一标识
        //商户商品名称，标的名称
        paramMap.put("agentGoodsName", lend.getTitle());
        //还款计划批次订单号
        paramMap.put("agentBatchNo",lendReturn.getReturnNo());
        //还款人绑定协议号
        paramMap.put("fromBindCode", bindCode);
        //当前一个月的还款总额
        paramMap.put("totalAmt", lendReturn.getTotal());
        paramMap.put("note", "");
        //还款明细，通过对应这条还款计划的id获取所有回款计划，就是当前这个月每个投资人的回款明细
        //通过还款计划的id，找到对应的回款计划数据，组装data参数中的需要的List<Map>集合
        List<Map<String, Object>> lendItemReturnDetailList = lendItemReturnService.addReturnDetail(lendReturnId);
        paramMap.put("data", JSONObject.toJSONString(lendItemReturnDetailList));//还款明细数据，json格式。
        paramMap.put("voteFeeAmt", new BigDecimal(0));//手续费
        paramMap.put("notifyUrl", HfbConst.BORROW_RETURN_NOTIFY_URL);//换款扣款完成后，异步通知商户扣款结果，回调汇付宝的接口
        paramMap.put("returnUrl", HfbConst.BORROW_RETURN_RETURN_URL);//还款扣款完成后，扣款结果同步返回到商户的完整地址
        paramMap.put("timestamp", RequestHelper.getTimestamp());
        String sign = RequestHelper.getSign(paramMap);
        paramMap.put("sign", sign);

        //构建自动提交表单
        String formStr = FormHelper.buildForm(HfbConst.BORROW_RETURN_URL, paramMap);
        return formStr;
    }

    /**
     * 对选中的这个期进行还款,汇付宝向尚融宝发送异步回调请求，并携带固定参数
     * 尚融宝业务：
     *  (1)更改还款计划表lend_return (status字段：状态 修改为1:已归还) (real_return_time字段：还款时间) (fee字段：手续费，是0，没有手续费，手续费在放款的时候就扣了)
     *  (2)更新标的信息：在最后一次还款后就更新标的状态，status字段修改为3已结清
     *  (3) 更新user_account表记录，对借款人账户扣除当月应还款的金额
     *  (4) 借款人交易流水
     *  (5) 更新回款计划表,更改回款状态（0-未归还 1-已归还）和 回款时间
     *  (6) 更新投资记录表(lend_item)的投资人实际收益(real_amount)。动态增加每个月都会还款都会增加利息
     *  (7) 更新user_account表，投资账号转入金额，给对应的投资人账户添加自己投资的金额+利息
     *  (8) 投资账号交易流水
     * @param paramMap 固定的参数
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void notify(Map<String, Object> paramMap) {

        log.info("还款成功");

        //还款编号
        String agentBatchNo = (String)paramMap.get("agentBatchNo");

        //幂等性判断
        boolean result = transFlowService.isSaveTransFlow(agentBatchNo);
        if(result){
            log.warn("幂等性返回");
            return;
        }

        //获取还款数据
        LambdaQueryWrapper<LendReturn> lendReturnQueryWrapper = new LambdaQueryWrapper<>();
        lendReturnQueryWrapper.eq(LendReturn::getReturnNo,agentBatchNo);
        LendReturn lendReturn = baseMapper.selectOne(lendReturnQueryWrapper);

        //(1) 更改还款计划表lend_return (status字段：状态 修改为1:已归还) (real_return_time字段：还款时间) (fee字段：手续费，是0，没有手续费，手续费在放款的时候就扣了)
        lendReturn.setStatus(1);
        String voteFeeAmt = (String)paramMap.get("voteFeeAmt");//手续费，是0，没有手续费，手续费在放款的时候就扣了
        lendReturn.setFee(new BigDecimal(voteFeeAmt));
        lendReturn.setRealReturnTime(LocalDateTime.now());//还款时间
        baseMapper.updateById(lendReturn);

        //(2) 更新标的信息
        Lend lend = lendMapper.selectById(lendReturn.getLendId());
        //如果是最后一次还款就更新标的状态
        //在放款的时候把最后一个月的last字段设置为了true，所有等最后一个月还款完成在设置标的状态
        if(lendReturn.getLast()) {
            lend.setStatus(LendStatusEnum.PAY_OK.getStatus());// PAY_OK(3, "已结清"),
            lendMapper.updateById(lend);
        }

        //(3) 更新user_account表记录，对借款人账户扣除当月应还款的金额
        //获取借款人bind_code
        String bindCodeByUserId = userBindService.getBindCodeByUserId(lendReturn.getUserId());
        //获取当月借款额度
        BigDecimal totalAmt = new BigDecimal((String)paramMap.get("totalAmt"));//还款金额
        userAccountMapper.updateAccount(bindCodeByUserId,totalAmt,new BigDecimal(0));

        //(4) 借款人交易流水
        TransFlowBO transFlowBO = new TransFlowBO(
                agentBatchNo,
                bindCodeByUserId,
                totalAmt,
                TransTypeEnum.RETURN_DOWN,
                "借款人还款扣减，项目编号：" + lend.getLendNo() + "，项目名称：" + lend.getTitle());
        transFlowService.saveTransFlow(transFlowBO);

        //获取回款明细列表
        List<LendItemReturn> lendItemReturnList = lendItemReturnService.selectLendItemReturnList(lendReturn.getId());
        lendItemReturnList.forEach(item -> {
            //(5) 更新回款计划表,更改回款状态（0-未归还 1-已归还）和 回款时间
            item.setStatus(1);
            item.setRealReturnTime(LocalDateTime.now());
            lendItemReturnMapper.updateById(item);

            //(6)、更新投资记录表(lend_item)的投资人实际收益(real_amount)。动态增加每个月都会还款都会增加利息
            LendItem lendItem = lendItemMapper.selectById(item.getLendItemId());
            lendItem.setRealAmount(lendItem.getRealAmount().add(item.getInterest()));
            lendItemMapper.updateById(lendItem);

            //(7)、更新user_account表，投资账号转入金额，给对应的投资人账户添加自己投资的金额+利息
            //获取投资人bind_code
            String investBindCode = userBindService.getBindCodeByUserId(item.getInvestUserId());
            userAccountMapper.updateAccount(investBindCode,item.getTotal(),new BigDecimal(0));

            //（8）投资账号交易流水
            TransFlowBO investTransFlowBO = new TransFlowBO(
                    LendNoUtils.getReturnItemNo(),
                    investBindCode,
                    item.getTotal(),
                    TransTypeEnum.INVEST_BACK,
                    "还款到账，项目编号：" + lend.getLendNo() + "，项目名称：" + lend.getTitle());
            transFlowService.saveTransFlow(investTransFlowBO);


        });

    }

    /**
     * 在网站端当前用户主页面，可查看的还款计划
     * @param page1
     * @param userId
     * @return
     */
    @Override
    public Page<LendReturn> selectLendReturn(Page<LendReturn> page1, Long userId) {
        LambdaQueryWrapper<LendReturn> lendReturnLambdaQueryWrapper = new LambdaQueryWrapper<>();
        lendReturnLambdaQueryWrapper.eq(LendReturn::getUserId,userId);

        Page<LendReturn> selectPage = baseMapper.selectPage(page1, lendReturnLambdaQueryWrapper);

        selectPage.getRecords().forEach(item ->{
            Lend lend = lendMapper.selectById(item.getLendId());
            UserInfo userInfo = userInfoMapper.selectById(item.getUserId());
            String returnMethod = dictService.getNameByParentDictCodeAndValue("returnMethod", item.getReturnMethod());
            item.getParam().put("userName",userInfo.getName());
            item.getParam().put("returnMethod",returnMethod);
            item.getParam().put("lendName",lend.getTitle());//标的名称
        });



        return selectPage;
    }
}
