package com.buba.yka.core.controller.api;


import com.alibaba.fastjson.JSON;
import com.buba.yka.base.utils.JwtUtils;
import com.buba.yka.common.result.R;
import com.buba.yka.core.hfb.RequestHelper;
import com.buba.yka.core.pojo.entity.UserBind;
import com.buba.yka.core.pojo.vo.UserBindVO;
import com.buba.yka.core.service.UserBindService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * <p>
 * 用户绑定表 前端控制器
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
@Api(tags = "会员账号绑定")
@RestController
@RequestMapping("/api/core/userBind")
@Slf4j
public class ApiUserBindController {

    @Resource
    private UserBindService userBindService;

    /**
     * 账号绑定，开户汇付包
     * 尚融宝新增新增了user_bind记录，汇付宝创建绑定账号（db_hfb数据库新建user_bind和user_account记录）生成bind_code字段
     * 账户绑定提交到托管平台（汇付包）的数据，以表单的形式自动提交调用汇付包接口，尚融宝对汇付包发送请求
     * @param userBindVO 绑定的表单数据
     * @param request 当前登录的token
     * @return 给前端返回一个html，表单自动提交，生成一个动态表单的字符串，表单参数：账户绑定的数据提交到托管平台的，请求的是汇付包的接口
     */
    @ApiOperation("账户绑定提交数据")
    @PostMapping("/auth/bind")
    public R bind(@RequestBody UserBindVO userBindVO, HttpServletRequest request){
        String token = request.getHeader("token");
        //从header中获取token，并对token进行校验，确保用户已登录，并从token中提取userId
        Long userId = JwtUtils.getUserId(token);//从token中提取userId，并进行了校验

        //根据userId做账号绑定，给当前登录的用户进行账号绑定，生成一个动态表单的字符串，账户绑定提交到托管平台的数据
        String formStr = userBindService.commitBindUser(userBindVO,userId);

        //生成一个动态表单的字符串，账户绑定提交到托管平台的数据，给前端返回一个html，表单自动提交
        return R.ok().data("formStr", formStr);

    }


    /**
     * 账户绑定成功异步回调，与汇付宝数据同步，汇付包对尚融宝发送的该请求，携带固定参数
     * //对尚融宝数据库进行修改绑定状态（user_info,user_bind）
     * 尚融宝user_bind表更新bind_code字段、status字段
     * 尚融宝user_info表更新 bind_code字段、name字段、idCard字段、bind_status字段
     * 尚融宝和汇付包通过（尚融宝user_info表的bind_code字段和user_bind表bind_code字段）
     * 和（汇付包的user_bind表中bind_code字段和user_account表中user_code字段）进行统一绑定，绑定成功
     * @param request 汇付包对尚融宝发送请求携带的参数
     * @return 尚融宝对汇付包返回success，返回success汇付包就不会发送重试了（不会报错了）成功必须返回success
     */
    @ApiOperation("账户绑定成功异步回调")
    @PostMapping("/notify")
    public String notify(HttpServletRequest request) {
        //汇付包向尚融宝发起回调请求时携带的参数，详细看汇付包文档 3.3.4同步返回/异步通知
        Map<String, Object> paramMap = RequestHelper.switchMap(request.getParameterMap());
        log.info("用户账号绑定异步回调：" + JSON.toJSONString(paramMap));
        //用户账号绑定异步回调接收的参数如下：{"agentUserId":"1",：账户在商户的会员用户ID
        // "resultCode":"0001","：结果编码。0001=绑定成功且通过实名认证、E105=绑定失败、U999=未知错误
        // sign":"7d3e729f3e89ae7ec92eea13250d000a",：签名
        // "bindCode":"c55e19c869a6499794934df41d3f02e8",:绑定账户协议号
        // "resultMsg":"成功",:绑定结果描述
        // "timestamp":"1690784688881"：通知时间。从1970-01-01 00:00:00算起的毫秒数。绑定错误不返回。
        // }


        //校验签名
        if(!RequestHelper.isSignEquals(paramMap)) {
            log.error("用户账号绑定异步回调签名错误：" + JSON.toJSONString(paramMap));
            //失败随便返回字符串
            return "fail";
        }

        //对尚融宝数据库进行修改绑定状态（user_info,user_bind）
        userBindService.notifyUserBind(paramMap);

        //尚融宝对汇付包返回success，返回success汇付包就不会发送重试了（不会报错了）成功必须返回success
        return "success";
    }
}

