package com.buba.yka.core;

import com.buba.yka.base.utils.JwtUtils;
import com.buba.yka.core.mapper.DictMapper;
import com.buba.yka.core.pojo.entity.Dict;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;


import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;


/**
 * @ClassName:RedisTemplateTests
 * @Auther: YooAo
 * @Description:
 * @Date: 2023/7/19 11:48
 * @Version: v1.0
 */
@SpringBootTest
@RunWith(SpringRunner.class)//连接spring上下文对象，这样就可以获取直接获取IOC容器中bean
public class RedisTemplateTests {
    @Resource
    private RedisTemplate redisTemplate;

    @Resource
    private DictMapper dictMapper;

    @Test
    public void saveDict(){
        Dict dict = dictMapper.selectById(1);
        System.out.println("dict");
        //向数据库中存储string类型的键值对, 过期时间5分钟
        redisTemplate.opsForValue().set("dict", dict, 5, TimeUnit.MINUTES);
        redisTemplate.opsForValue().set("dict2", "dict");

    }

    @Test
    public void getDict(){
        Dict dict = (Dict)redisTemplate.opsForValue().get("dict");
        System.out.println(dict);
    }


    //eyJhbGciOiJIUzUxMiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAKtWKi5NUrJSCg5y0g0Ndg1S0lFKrShQsjI0szQwNjexsDDRUSotTi3yTAGKQZh-ibmpQB2GRsZKtQCOF-xCPwAAAA.buhMOPEZM08hATSggNLQ2UAzbiskmuR7DHMq5EwsGomQZfVvuqvZm2QH4RTMyw2h1lZPoWUfd5s7xX4cuRunhw
}
