package com.buba.yka.core.controller.admin;


import com.alibaba.excel.EasyExcel;
import com.buba.yka.common.exception.BusinessException;
import com.buba.yka.common.result.R;
import com.buba.yka.common.result.ResponseEnum;
import com.buba.yka.core.pojo.dto.ExcelDictDTO;
import com.buba.yka.core.pojo.entity.Dict;
import com.buba.yka.core.service.DictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.List;

/**
 * <p>
 * 数据字典 前端控制器，admin后台管理系统的接口
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
@Api(tags = "数据字典后台管理")
@RestController
@RequestMapping("/admin/core/dict")
@Slf4j
//@CrossOrigin//解决跨域问题，笔记在mvc ajax中
public class AdminDictController {

    @Resource
    private DictService dictService;


    @ApiOperation("Excel批量导入（读）数据字典数据库")
    @PostMapping("/import")
    public R batchImport( @ApiParam(value = "Excel文件", required = true) @RequestParam("file") MultipartFile file) {

        try {
            //InputStream 是 JavaIO 接口的一个类，用于读取数据。它可以用来读取字符串、字节数组、文件等数据类型。
            //InputStream：输入流。上传文件从硬盘读到内存中
            InputStream inputStream = file.getInputStream();

            System.out.println("inputStream："+inputStream);

            dictService.importData(inputStream);

            return R.ok().message("批量导入成功");
        } catch (Exception e) {
            //UPLOAD_ERROR(-103, "文件上传错误"),
            throw new BusinessException(ResponseEnum.UPLOAD_ERROR, e);
        }
    }


    @ApiOperation("数据导出Excel（写）")
    @GetMapping("/export")
    public void export(HttpServletResponse response){

        try {
            // 这里注意 有同学反应使用swagger 会导致各种问题，请直接用浏览器或者用postman
            response.setContentType("application/vnd.ms-excel");//生成excel格式

            response.setCharacterEncoding("utf-8");//编码方式utf-8

            // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
            String fileName = URLEncoder.encode("mydict", "UTF-8").replaceAll("\\+", "%20");

            //Content-disposition：下载文件到本地
            response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");

            // 这里 需要指定写用哪个class去写，然后写到第一个sheet，名字为数据字典 然后文件流会自动关闭
            //doWrite(data())：把data返回的数据写到response.getOutputStream()指定目录下sheet叫数据字典生成excel
            //dictService.listDictData()数据的返回值与ExcelDictDTO.class保持一致
            //response.getOutputStream()：输出流（写）从内存中输出到硬盘中
            EasyExcel.write(response.getOutputStream(), ExcelDictDTO.class).sheet("数据字典").doWrite(dictService.listDictData());

        } catch (IOException e) {
            //EXPORT_DATA_ERROR(104, "数据导出失败"),
            throw  new BusinessException(ResponseEnum.EXPORT_DATA_ERROR, e);
        }
    }




    /*树形数据的两种加载方案
    方案一:非延迟加载
    需要后端返回的数据结构中包含套数据，并且嵌套数据放在children属性中

    方案二:延迟加载
    不需要后端返回数据中包含套数据，并且要定义布尔属性hasChildren表示当前节点是否包含子数据
    如果hasChildren为true，就表示当前节点包含子数据
    如果hasChildren为false，就表示当前节点不包含子数据
    如果当前节点包含子数据，那么点击当前节点的时候，就需要通过Load方法加载子数据
    */
    /**
     * 字典列表展示，使用了方案二
     * 前端页面初始化请求parentId为1的返回数据列表，在点击父级为1的id（20000，30000）请求展示二级列表
     * 将数据字典列表数据存入Redis，在长期不改变表的数据的前提下存入redis，可以提高查询效率，提高性能
     * 先查询redis中，如果没有数据就从数据库中取值，如果有就从redis中取值，效率非常高
     * @param parentId
     * @return
     */
    @ApiOperation("根据上级id获取子节点数据列表")
    @GetMapping("/listByParentId/{parentId}")
    public R listByParentId(
            @ApiParam(value = "上级节点id", required = true)
            @PathVariable Long parentId) {
        List<Dict> dictList = dictService.listByParentId(parentId);
        return R.ok().data("list", dictList);
    }

}

