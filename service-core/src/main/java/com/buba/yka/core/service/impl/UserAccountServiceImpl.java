package com.buba.yka.core.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.buba.yka.base.dto.SmsDTO;
import com.buba.yka.common.exception.Assert;
import com.buba.yka.common.result.ResponseEnum;
import com.buba.yka.core.enums.TransTypeEnum;
import com.buba.yka.core.hfb.FormHelper;
import com.buba.yka.core.hfb.HfbConst;
import com.buba.yka.core.hfb.RequestHelper;
import com.buba.yka.core.mapper.UserAccountMapper;
import com.buba.yka.core.mapper.UserInfoMapper;
import com.buba.yka.core.pojo.bo.TransFlowBO;
import com.buba.yka.core.pojo.entity.UserAccount;
import com.buba.yka.core.pojo.entity.UserInfo;
import com.buba.yka.core.service.TransFlowService;
import com.buba.yka.core.service.UserAccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.buba.yka.core.service.UserBindService;
import com.buba.yka.core.service.UserInfoService;
import com.buba.yka.core.utils.LendNoUtils;
import com.buba.yka.rabbitutil.constant.MQConst;
import com.buba.yka.rabbitutil.service.MQService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 用户账户 服务实现类
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
@Service
@Slf4j
public class UserAccountServiceImpl extends ServiceImpl<UserAccountMapper, UserAccount> implements UserAccountService {

    @Resource
    private UserInfoMapper userInfoMapper;

    @Resource
    private TransFlowService transFlowService;

    @Resource
    private UserBindService userBindService;

    @Resource
    private UserInfoService userInfoService;

    @Resource
    private MQService mqService;//rabbit-mq服务模块中的

    /**
     * 账户充值，尚融宝对汇付宝发送请求，携带着固定参数（汇付宝要求的）
     * 与账户绑定流程一样（ApiUserBindController）
     *         //之前用户账号绑定时，汇付宝向尚融宝发送的异步请求参数中有绑定协议号（bind_code），添加到user_info表中和user_bind表中了。
     *         //所以获取当前登录的用户的bind_code，发送给汇付宝与汇付宝的bind_code进行统一绑定，
     *         //汇付宝通过bind_code获取到user_bind表中的用户绑定信息，在对指定的bind_code的用户进行充值
     *         汇付包user_account表更新user_account记录中的amount的值
     * @param chargeAmt 充值金额
     * @param userId 当前登录用户id
     * @return 给前端返回一个动态html，表单自动提交，生成一个动态表单的字符串，表单携带着固定参数：表单自动提交对汇付宝发送请求
     */
    @Override
    public String commitCharge(BigDecimal chargeAmt, Long userId) {

        //当前登录的用户对象
        UserInfo userInfo = userInfoMapper.selectById(userId);
        // 获取之前账户与汇付宝绑定的bind_code绑定协议号。
        String bindCode = userInfo.getBindCode();
        //判断账户绑定状态，如果为空USER_NO_BIND_ERROR(302, "用户未绑定"),
        Assert.notEmpty(bindCode, ResponseEnum.USER_NO_BIND_ERROR);

        //组装表单参数，对汇付宝请求携带的固定参数
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("agentId", HfbConst.AGENT_ID);//给商户分配的唯一标识
        paramMap.put("agentBillNo", LendNoUtils.getNo());//商户充值单号（要求唯一）随机生成的流水号
        //充值人绑定协议号。
        //之前用户账号绑定时，汇付宝向尚融宝发送的异步请求参数中有绑定协议号（bind_code），添加到user_info表中和user_bind表中了。
        //所以获取当前登录的用户的bind_code，发送给汇付宝与汇付宝的bind_code进行统一绑定，
        //汇付宝通过bind_code获取到user_bind表中的用户绑定信息，在对指定的bind_code的用户进行充值
        paramMap.put("bindCode", bindCode);
        paramMap.put("chargeAmt", chargeAmt);//充值金额，即充值到汇付宝的金额。支持小数点后2位。
        //商户收取用户的手续费。支持小数点后2位。一般就传0，充值不需要手续费
        //注意：从银行卡扣除金额= charge_amt+ fee_amt，用户汇付宝余额增加charge_amt，fee_amt转到商户账户。
        paramMap.put("feeAmt", new BigDecimal("0"));
        paramMap.put("notifyUrl", HfbConst.RECHARGE_NOTIFY_URL);//异步通知商户充值成功的完整地址（汇付宝对尚融宝发送请求，携带固定的参数）
        paramMap.put("returnUrl", HfbConst.RECHARGE_RETURN_URL);//充值完成后同步返回商户的完整地址。返回尚融宝页面的路径
        paramMap.put("timestamp", RequestHelper.getTimestamp());//时间戳
        String sign = RequestHelper.getSign(paramMap);
        paramMap.put("sign", sign);//获取加密后的签名，有固定的格式加密，可以进去看看

        //构建充值自动提交表单，用表单形式提交到汇付包，参数是 paramMap map集合 表单带有自动提交
        // HfbConst.USERBIND_URL：提交地址，访问汇付包的接口
        //paramMap：携带的参数
        //formStr：生成了一个动态的表单
        String formStr = FormHelper.buildForm(HfbConst.RECHARGE_URL, paramMap);

        return formStr;
    }


    /**
     * 账户充值异步回调，与汇付宝数据同步，汇付宝向尚融宝发送请求，并携带固定参数
     * 目的：
     * （1）尚融宝账户金额更改（user_account）
     * （2）尚融宝添加交易流水（trans_flow）
     * 流程：
     * 汇付宝充值成功后异步通知到尚融宝携带固定参数有bind_code,尚融宝在通过bind_code查询user_info表的用户id，在通过用户id更新user_account表，把充值金额同步到尚融宝中(更新user_account表的amount(账户可用金额)和freeze_amount(冻结金额)字段)
     * @param paramMap 获取汇付宝向尚融宝发送的参数
     * @return 尚融宝对汇付包返回success，返回success汇付包就不会发送重试了（不会报错了）成功必须返回success
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public String notify(Map<String, Object> paramMap) {


        //幂等性判断，解决的问题：当回调重试时，金额和流水会重复增加
        //汇付宝向尚融宝发起回调（汇付宝发送异步通知），但是返回结果的时候网络异常，或返回的不是success,如果没有收到正确的响应 "success"，则汇付宝会发起重试
        //注意：是先处理完业务后在return(响应)的，所以第一次业务是执行完成的（下面(1)(2)是执行完成的），在次发起重试，重试的参数是一样的，这样会导致处理了多次业务，所以这样金额和流水会重复增加
        //所以要判断交易流水是否存在，因为交易订单流水号是唯一标识添加了约束Unique
        String agentBillNo = (String)paramMap.get("agentBillNo"); //获取商户充值订单号流水号
        boolean saveTransFlow = transFlowService.isSaveTransFlow(agentBillNo);
        //如果重试了，则先判断流水号是否存在，如果存在则返回success从业务方法中直接退出，就不会重试了
        if(saveTransFlow){
            log.warn("幂等性返回");
            return "success";
        }

        //（1）尚融宝账户金额更改（user_account）
        //账户处理，把充值的钱充到汇付宝了（user_account），在把充值金额同步到尚融宝(user_account，trans_flow表)
        String bindCode = (String)paramMap.get("bindCode"); //充值人绑定协议号
        String chargeAmt = (String)paramMap.get("chargeAmt"); //充值金额
        //汇付宝充值成功后异步通知到尚融宝携带固定参数有bind_code,尚融宝在通过bind_code查询user_info表的用户id，
        //在通过用户id更新user_account表，把充值金额同步到尚融宝中(更新user_account表的amount(账户可用金额)和freeze_amount(冻结金额)字段)
        baseMapper.updateAccount(bindCode, new BigDecimal(chargeAmt), new BigDecimal(0));

        //（2）尚融宝添加交易流水（trans_flow）
        TransFlowBO transFlowBO = new TransFlowBO();
        transFlowBO.setAmount(new BigDecimal(chargeAmt));//设置金额
        transFlowBO.setAgentBillNo(agentBillNo);//设置交易单号，流水号
        transFlowBO.setBindCode(bindCode);//设置充值人绑定协议号
        transFlowBO.setTransTypeEnum(TransTypeEnum.RECHARGE);//设置交易类型，RECHARGE(1,"充值"),
        transFlowBO.setMemo("用户充值");//描述
        transFlowService.saveTransFlow(transFlowBO);

//        //当前用户充值完成后给当前用户手机号发送充值成功的短信
        log.info("发消息");
        String mobile = userInfoService.getMobileByBindCode(bindCode);//获取到当前用户的手机号
        SmsDTO smsDTO = new SmsDTO();
        smsDTO.setMobile(mobile);
        smsDTO.setMessage("1111");
        //发送短信，调用rabbit-mq模块中的方法，参数：交换机、路由、发送的消息
        mqService.sendMessage(MQConst.EXCHANGE_TOPIC_SMS, MQConst.ROUTING_SMS_ITEM, smsDTO);

        return "success";
    }


    /**
     * 通过用户id查询当前投资人账户余额
     * @param userId 用户id
     * @return 余额
     */
    @Override
    public BigDecimal getAccount(Long userId) {
        LambdaQueryWrapper<UserAccount> userAccountLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userAccountLambdaQueryWrapper.eq(UserAccount::getUserId,userId);
        UserAccount userAccount = baseMapper.selectOne(userAccountLambdaQueryWrapper);

        return userAccount.getAmount();
    }



    /**
     * 用户提现，提现到银行（因为没有银行所以，所以是虚拟的），尚融宝调用汇付宝并携带固定参数
     * 汇付宝业务：更改user_account表，当前用户账户金额减去提现金额（amount字段）
     * @param fetchAmt 提现金额
     * @param userId 当前用户
     * @return
     */
    @Override
    public String commitWithdraw(BigDecimal fetchAmt, Long userId) {
        //账户可用余额充足：当前用户的余额 >= 当前用户的提现金额
        BigDecimal amount = this.getAccount(userId);//获取当前用户的账户余额
        Assert.isTrue(amount.doubleValue() >= fetchAmt.doubleValue(),
                ResponseEnum.NOT_SUFFICIENT_FUNDS_ERROR);

        //查询当前用户bind_code
        String bindCode = userBindService.getBindCodeByUserId(userId);

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("agentId", HfbConst.AGENT_ID);//给商户分配的唯一标识
        paramMap.put("agentBillNo", LendNoUtils.getWithdrawNo());//商户提现单号流水号。
        paramMap.put("bindCode", bindCode);//用户绑定协议号
        paramMap.put("fetchAmt", fetchAmt);//提现金额。
        paramMap.put("feeAmt", new BigDecimal(0));//商户收取用户的手续费
        paramMap.put("notifyUrl", HfbConst.WITHDRAW_NOTIFY_URL);///用户绑定异步回调交给尚融宝处理，给尚融宝发送该请求
        paramMap.put("returnUrl", HfbConst.WITHDRAW_RETURN_URL);//用户绑定同步回调同步返回到user界面
        paramMap.put("timestamp", RequestHelper.getTimestamp());
        String sign = RequestHelper.getSign(paramMap);
        paramMap.put("sign", sign);

        //构建自动提交表单
        String formStr = FormHelper.buildForm(HfbConst.WITHDRAW_URL, paramMap);
        return formStr;
    }

    /**
     * 用户提现异步回调，汇付宝请求尚融宝接口并携带参数，把数据同步到尚融宝
     * 尚融宝业务：更改user_account表，当前用户账户金额减去提现金额（amount字段），增加交易流水(trans_flow表)
     * @param paramMap 携带的参数
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void notifyWithdraw(Map<String, Object> paramMap) {

        log.info("提现成功");
        String agentBillNo = (String)paramMap.get("agentBillNo");//流水号
        boolean result = transFlowService.isSaveTransFlow(agentBillNo);
        if(result){
            log.warn("幂等性返回");
            return;
        }

        String bindCode = (String)paramMap.get("bindCode");
        String fetchAmt = (String)paramMap.get("fetchAmt");

        //根据用户账户修改账户金额，当前账户金额 - 提现金额
        baseMapper.updateAccount(bindCode, new BigDecimal("-" + fetchAmt), new BigDecimal(0));

        //增加交易流水
        TransFlowBO transFlowBO = new TransFlowBO(
                agentBillNo,
                bindCode,
                new BigDecimal(fetchAmt),
                TransTypeEnum.WITHDRAW,
                "提现");
        transFlowService.saveTransFlow(transFlowBO);
    }
}
