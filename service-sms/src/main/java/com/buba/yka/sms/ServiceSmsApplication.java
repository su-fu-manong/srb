package com.buba.yka.sms;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
//swagerr配置在base服务中了所以启动这个服务就可以访问base服务配置的swagerr，还有common模块的异常类,因为这两个服务没有启动类扫扫不到，无法装配到spring容器中
@ComponentScan({"com.buba.yka"})//启动这个服务时扫描所有这个包下的服务模块的组件到ioc容器中,只要有是这个包名的服务模块都能访问到
@EnableTransactionManagement //事务处理
@EnableFeignClients//开启Fegin
public class ServiceSmsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceSmsApplication.class, args);

    }
}