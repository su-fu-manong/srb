package com.buba.yka.core.controller.api;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.buba.yka.common.result.R;
import com.buba.yka.core.pojo.entity.Lend;
import com.buba.yka.core.service.LendService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 标的准备表 前端控制器
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
@Api(tags = "标的")
@RestController
@RequestMapping("/api/core/lend")
@Slf4j
public class ApiLendController {

    @Resource
    private LendService lendService;

    @ApiOperation("获取标的列表")
    @GetMapping("/list/{page}/{limit}")
    public R list(
            @ApiParam(value = "当前页码", required = true)
            @PathVariable Long page,

            @ApiParam(value = "每页记录数", required = true)
            @PathVariable Long limit
    ) {
        Page<Lend> page1 = new Page<>(page,limit);
        Page<Lend> lendList = lendService.selectList(page1);
        return R.ok().data("lendList", lendList);
    }


    /**
     * 获取标的详细信息
     * 目的：
     * 1、获取标的详细信息
     * 2、获取借款人详细信息
     * @param id 标的id
     * @return
     */
    @ApiOperation("获取标的详细信息")
    @GetMapping("/show/{id}")
    public R show(
            @ApiParam(value = "标的id", required = true)
            @PathVariable Long id) {
        //通过标的id获取标的详细信息，和 借款人详细信息
        Map<String, Object> lendDetail = lendService.getLendDetail(id);
        return R.ok().data("lendDetail", lendDetail);
    }


    /**
     * 计算投资所获收益，所获总利息
     * @param invest 投资金额
     * @param yearRate 年化收益
     * @param totalmonth 期数（几个月后还款）
     * @param returnMethod 还款方式 1-等额本息 2-等额本金 3-每月还息一次还本 4-一次还本
     * @return 所获收益，所获总利息
     */
    @ApiOperation("计算投资收益")
    @GetMapping("/getInterestCount/{invest}/{yearRate}/{totalmonth}/{returnMethod}")
    public R getInterestCount(
            @ApiParam(value = "投资金额", required = true)
            @PathVariable("invest") BigDecimal invest,

            @ApiParam(value = "年化收益", required = true)
            @PathVariable("yearRate")BigDecimal yearRate,

            @ApiParam(value = "期数", required = true)
            @PathVariable("totalmonth")Integer totalmonth,

            @ApiParam(value = "还款方式", required = true)
            @PathVariable("returnMethod")Integer returnMethod) {

        BigDecimal interestCount = lendService.getInterestCount(invest, yearRate, totalmonth, returnMethod);
        return R.ok().data("interestCount", interestCount);
    }

}

