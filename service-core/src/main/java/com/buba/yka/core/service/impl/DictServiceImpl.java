package com.buba.yka.core.service.impl;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.buba.yka.core.listener.ExcelDictDTOListener;
import com.buba.yka.core.mapper.DictMapper;
import com.buba.yka.core.pojo.dto.ExcelDictDTO;
import com.buba.yka.core.pojo.entity.Dict;
import com.buba.yka.core.service.DictService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 数据字典 服务实现类
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
@Slf4j
@Service
public class DictServiceImpl extends ServiceImpl<DictMapper, Dict> implements DictService {

    @Resource
    private RedisTemplate redisTemplate;

    /**
     * excel导入到数据库
     * @param inputStream
     */
    //开启事务出现所有异常回滚数据
    @Transactional(rollbackFor = {Exception.class})
    @Override
    public void importData(InputStream inputStream) {
        // 这里 需要指定读用哪个class去读，然后读取第一个sheet 文件流会自动关闭
        //doRead()：把inputStream指定路径下的excel表格的数据读到ExcelStudentDTOListener监听器进行解析，返回ExcelDictDTO实体类
        //baseMapper：就相当于DictMapper dictMapper，把mapper对象传到监听器中
        EasyExcel.read(inputStream, ExcelDictDTO.class, new ExcelDictDTOListener(baseMapper)).sheet().doRead();

        log.info("inputStream {}",inputStream);
        log.info("excel导入完毕");
    }


    /**
     * 数据导出到excel，返回值与ExcelDictDTO.class保持一致
     * @return
     */
    @Override
    public List<ExcelDictDTO> listDictData() {

        //查询数据字典所有数据
        List<Dict> dictList = baseMapper.selectList(null);

        //创建ExcelDictDTO列表，将Dict列表转换成ExcelDictDTO列表，dictList.size()：固定集合长度
        ArrayList<ExcelDictDTO> excelDictDTOList = new ArrayList<>(dictList.size());
        dictList.forEach(dict -> {

            ExcelDictDTO excelDictDTO = new ExcelDictDTO();

            // BeanUtils会使用反射机制获取两个对象的属性列表,找出属性名相同的属性对。然后将dict对象中相同名称属性的值赋值给dictDTO对应的属性。
            BeanUtils.copyProperties(dict, excelDictDTO);//对象拷贝

            excelDictDTOList.add(excelDictDTO);
        });
        return excelDictDTOList;
    }


    /**
     * 根据上级id获取子节点数据列表，如果有子节点就把当前父节点设置hasChildren字段true
     * @param parentId 父id
     *前端页面初始化请求parentId为1的返回数据列表，在点击父级为1的id（20000，30000）请求展示二级列表
     *
     * 例子：看数据库，假设传过来的父id为1，他会查询parent_id字段为1的数据，就是一级目录。前端展示一级目录
     * 循环一级目录，判断一级目录是否有二级目录（子级），
     * 把一级目录的id（20000，30000，40000...）传到 hasChildren() 方法中，判断当前节点是否有子节点，
     * 查询parent_id字段为当前id（20000，30000，40000...）的数据，如果存在说明当前id有子级，则返回true，如果不存在则返回false
     * 如果返回true则有子节点，就把当前父节点为1的设置hasChildren字段true
     * @return
     */
    @Override
    public List<Dict> listByParentId(Long parentId) {
        //先查询redis中是否存在数据列表
        try {
            List<Dict>  dictList = (List<Dict>)redisTemplate.opsForValue().get("srb:core:dictList:" + parentId);
            //如果存在则从redis中直接返回数据类型
            if(dictList != null){
                log.info("从redis中取值");
                return dictList;
            }
        } catch (Exception e) {
            //此处不抛出异常，如果redis出现异常，继续执行后面的代码，从数据库中取值
            log.error("redis服务器异常：" + ExceptionUtils.getStackTrace(e));
        }

        //如果不存在或redis报错从则查询数据库中返回数据
        // SELECT * FROM dict WHERE is_deleted=0 AND (parent_id = ?)
        LambdaQueryWrapper<Dict> parent_id = new LambdaQueryWrapper<>();
        parent_id.eq(Dict::getParentId, parentId);

        //查询父级id为前端传过来的id的所有数据
        List<Dict> dictList = baseMapper.selectList(parent_id);

        //填充hasChildren字段
        //如果hasChildren为true，就表示当前节点包含子数据
        //如果hasChildren为false，就表示当前节点不包含子数据
        dictList.forEach(dict -> {
            //判断当前节点是否有子节点，如果返回true则有子节点，就把当前父节点设置hasChildren字段true
            boolean hasChildren = this.hasChildren(dict.getId());
            dict.setHasChildren(hasChildren);
        });

        //将数据存入redis,五分钟过期
        try {
            redisTemplate.opsForValue().set("srb:core:dictList:" + parentId, dictList, 5, TimeUnit.MINUTES);
            log.info("数据存入redis");
        } catch (Exception e) {
            log.error("redis服务器异常：" + ExceptionUtils.getStackTrace(e));//此处不抛出异常，继续执行后面的代码
        }
        return dictList;
    }


    /**
     * 判断当前id是否有子节点，就看其它数据的父id是不是当前这个id
     */
    private boolean hasChildren(Long id) {
        LambdaQueryWrapper<Dict> queryWrapper = new LambdaQueryWrapper<Dict>().eq(Dict::getParentId, id);
        Long count = baseMapper.selectCount(queryWrapper);
        if(count.intValue() > 0) {
            return true;
        }
        return false;
    }


    /**
     * 根据dictCode编码获取下级节点
     * @param dictCode 数据字典的编码，dictCode字段
     * @return
     */
    @Override
    public List<Dict> findByDictCode(String dictCode) {
        //根据dictCode编码查询当前id
        LambdaQueryWrapper<Dict> dictLambdaQueryWrapper = new LambdaQueryWrapper<>();
        dictLambdaQueryWrapper.eq(Dict::getDictCode,dictCode);
        Dict dict = baseMapper.selectOne(dictLambdaQueryWrapper);

        //通过当前的id当父id查询，查询出当前id的所有子级节点
        List<Dict> dicts = this.listByParentId(dict.getId());
        return dicts;
    }

    /**
     * 通过表中编码字段和value字段查询对应的名称
     * @param dictCode 编码
     * @param value value
     * @return
     */
    @Override
    public String getNameByParentDictCodeAndValue(String dictCode, Integer value) {

        //先查询对应的编码数据
        LambdaQueryWrapper<Dict> dictLambdaQueryWrapper = new LambdaQueryWrapper<>();
        dictLambdaQueryWrapper.eq(Dict::getDictCode,dictCode);
        Dict parentDict = baseMapper.selectOne(dictLambdaQueryWrapper);

        if(parentDict == null) {
            return "";
        }

        //在通过查询出的id，作为父id来查询对应的value的子节点数据
        dictLambdaQueryWrapper = new LambdaQueryWrapper<>();

        dictLambdaQueryWrapper.eq(Dict::getParentId,parentDict.getId()).eq(Dict::getValue,value);
        Dict dict = baseMapper.selectOne(dictLambdaQueryWrapper);

        if(dict == null) {
            return "";
        }

        //返回对应的名称
        return dict.getName();


    }


}

