package com.buba.yka.core.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.buba.yka.core.pojo.entity.BorrowInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 借款信息表 Mapper 接口
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
public interface BorrowInfoMapper extends BaseMapper<BorrowInfo> {

    Page<BorrowInfo> selectBorrowInfoList(@Param("pageParam") Page<BorrowInfo> pageParam, @Param("keyword") String keyword);
}
