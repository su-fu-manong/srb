package com.buba.yka.core.service.impl;

import com.alibaba.excel.util.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.buba.yka.base.utils.JwtUtils;
import com.buba.yka.common.exception.Assert;
import com.buba.yka.common.result.ResponseEnum;
import com.buba.yka.common.utils.MD5;
import com.buba.yka.core.mapper.UserAccountMapper;
import com.buba.yka.core.mapper.UserInfoMapper;
import com.buba.yka.core.mapper.UserLoginRecordMapper;
import com.buba.yka.core.pojo.entity.UserAccount;
import com.buba.yka.core.pojo.entity.UserInfo;
import com.buba.yka.core.pojo.entity.UserLoginRecord;
import com.buba.yka.core.pojo.query.UserInfoQuery;
import com.buba.yka.core.pojo.vo.LoginVO;
import com.buba.yka.core.pojo.vo.RegisterVO;
import com.buba.yka.core.pojo.vo.UserIndexVO;
import com.buba.yka.core.pojo.vo.UserInfoVO;
import com.buba.yka.core.service.UserAccountService;
import com.buba.yka.core.service.UserInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;

/**
 * <p>
 * 用户基本信息 服务实现类
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements UserInfoService {

    @Resource
    private UserAccountMapper userAccountMapper;

    @Resource
    private UserLoginRecordMapper userLoginRecordMapper;

    /**
     * 用户界面，会员注册，新增用户基本信息，用户账户消息
     * @param registerVO 用户消息
     */
    @Transactional(rollbackFor = {Exception.class})
    @Override
    public void register(RegisterVO registerVO) {

        //判断用户是否被注册
        LambdaQueryWrapper<UserInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserInfo::getMobile, registerVO.getMobile());
        Long count = baseMapper.selectCount(queryWrapper);
        //如果count不等于0，抛出异常MOBILE_EXIST_ERROR(-207, "手机号已被注册"),
        Assert.isTrue(count == 0, ResponseEnum.MOBILE_EXIST_ERROR);

        //插入用户基本信息 userInfo
        UserInfo userInfo = new UserInfo();
        userInfo.setUserType(registerVO.getUserType());
        userInfo.setNickName(registerVO.getMobile());
        userInfo.setName(registerVO.getMobile());
        userInfo.setMobile(registerVO.getMobile());
        //MD5加密后的密码
        String password = DigestUtils.md5DigestAsHex(registerVO.getPassword().getBytes());
        userInfo.setPassword(password);
        userInfo.setStatus(UserInfo.STATUS_NORMAL); //正常
        //设置一张静态资源服务器上的头像图片
        userInfo.setHeadImg("https://srb-file-yka.oss-cn-beijing.aliyuncs.com/userImage/2023/07/25/a10a671b-26b7-4475-8340-42b85c1fc52b.jpg");
        baseMapper.insert(userInfo);

        //记录用户账户 userAccount
        UserAccount userAccount = new UserAccount();
        userAccount.setUserId(userInfo.getId());
        userAccountMapper.insert(userAccount);
    }


    /**
     * 用户界面，用户登录，校验登录，新增记录登录日志user_login_record，每登录一次都会记录一次日志。生成token，组织UserInfoVO返回前端
     * @param loginVO 登录消息
     * @param ip  需要获取远程主机客户端IP地址，记录登录日志表user_login_record填写IP
     * @return
     */
    @Transactional(rollbackFor = {Exception.class})
    @Override
    public UserInfoVO login(LoginVO loginVO, String ip) {

        Integer userType = loginVO.getUserType();//用户类型
        String mobile = loginVO.getMobile();//手机号
        String password = loginVO.getPassword();//密码

        //用户是否存在
        LambdaQueryWrapper<UserInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserInfo::getMobile,mobile).eq(UserInfo::getUserType,userType);
        UserInfo userInfo = baseMapper.selectOne(queryWrapper);
        //如果为空，则抛出异常LOGIN_MOBILE_ERROR(208, "用户不存在"),
        Assert.notNull(userInfo,ResponseEnum.LOGIN_MOBILE_ERROR);


        //密码是否正确
        //把前端传过来的密码加密与数据库中加密的密码进行比较
        String MD5Password = DigestUtils.md5DigestAsHex(password.getBytes());
        //把前端传过来的密码加密与数据库中加密的密码不一致则抛出异常，LOGIN_PASSWORD_ERROR(209, "密码错误"),
        Assert.equals(MD5Password,userInfo.getPassword(),ResponseEnum.LOGIN_PASSWORD_ERROR);


        //用户是否被禁用
        //如果查询到的用户状态不是1，说明被禁用了，则抛出LOGIN_DISABLED_ERROR(-210, "用户已被禁用"),
        Assert.equals(userInfo.getStatus(),UserInfo.STATUS_NORMAL,ResponseEnum.LOGIN_LOKED_ERROR);


        //新增记录登录日志，每登录一次都会记录一次日志
        UserLoginRecord userLoginRecord = new UserLoginRecord();
        userLoginRecord.setUserId(userInfo.getId());
        userLoginRecord.setIp(ip);
        userLoginRecordMapper.insert(userLoginRecord);


        //生成token，返回给前端，过期时间为一天，每次登录都会生成新的token重新计时
        //前端发送请求都必须携带token并校验token
        String token = JwtUtils.createToken(userInfo.getId(), userInfo.getName());


        //组装UserInfoVo返回给前端，存到cookie中
        UserInfoVO userInfoVO = new UserInfoVO();
        userInfoVO.setUserType(userType);
        userInfoVO.setMobile(mobile);
        userInfoVO.setName(userInfo.getName());
        userInfoVO.setNickName(userInfo.getNickName());
        userInfoVO.setHeadImg(userInfo.getHeadImg());
        userInfoVO.setToken(token);
        userInfoVO.setUserId(userInfo.getId());
        return userInfoVO;
    }

    /**
     * 后台管理系统，用户分页加筛选查询
     * @param pageParam 分页数据
     * @param userInfoQuery 筛选对象数据
     * @return
     */
    @Override
    public Page<UserInfo> listPage(Page<UserInfo> pageParam, UserInfoQuery userInfoQuery) {
        String mobile = userInfoQuery.getMobile();
        Integer status = userInfoQuery.getStatus();
        Integer userType = userInfoQuery.getUserType();

        LambdaQueryWrapper<UserInfo> userInfoQueryWrapper = new LambdaQueryWrapper<>();

        //如果筛选条件为空，就查询全部
        if(userInfoQuery == null){
            return baseMapper.selectPage(pageParam, null);
        }

        //有筛选查询的，查询指定用户信息，满足condition条件则查询，不满足当前不查询该sql语句
        userInfoQueryWrapper
                .eq(StringUtils.isNotBlank(mobile), UserInfo::getMobile, mobile)
                .eq(status != null, UserInfo::getStatus, userInfoQuery.getStatus())
                .eq(userType != null, UserInfo::getUserType, userType);
        return baseMapper.selectPage(pageParam, userInfoQueryWrapper);
    }

    /**
     * 后台管理系统，更改用户状态status，锁定或解锁，状态（0：锁定 1：正常）
     * @param id 用户id
     * @param status 用户更改的状态（0：锁定 1：正常），传1过来就是把当前用户解锁，0是把当前用户锁定
     */
    @Override
    public void lock(Long id, Integer status) {
        UserInfo userInfo = new UserInfo();
        userInfo.setId(id);
        userInfo.setStatus(status);
        //修改用户状态
        baseMapper.updateById(userInfo);
    }

    /**
     * 用户界面，校验手机号是否注册
     * @param mobile
     * @return
     */
    @Override
    public Boolean checkMobile(String mobile) {
        LambdaQueryWrapper<UserInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserInfo::getMobile,mobile);
        Long aLong = baseMapper.selectCount(queryWrapper);

        return aLong>0;
    }



    @Override
    public UserIndexVO getIndexUserInfo(Long userId) {
        //用户信息
        UserInfo userInfo = baseMapper.selectById(userId);

        //账户信息
        LambdaQueryWrapper<UserAccount> userAccountQueryWrapper = new LambdaQueryWrapper<>();
        userAccountQueryWrapper.eq(UserAccount::getUserId, userId);
        UserAccount userAccount = userAccountMapper.selectOne(userAccountQueryWrapper);

        //登录信息
        LambdaQueryWrapper<UserLoginRecord> userLoginRecordQueryWrapper = new LambdaQueryWrapper<>();
        userLoginRecordQueryWrapper
                .eq(UserLoginRecord::getUserId, userId)
                .orderByDesc(UserLoginRecord::getId)
                .last("limit 1");
        UserLoginRecord userLoginRecord = userLoginRecordMapper.selectOne(userLoginRecordQueryWrapper);

        //组装结果数据
        UserIndexVO userIndexVO = new UserIndexVO();
        userIndexVO.setUserId(userInfo.getId());
        userIndexVO.setUserType(userInfo.getUserType());
        userIndexVO.setName(userInfo.getName());
        userIndexVO.setNickName(userInfo.getNickName());
        userIndexVO.setHeadImg(userInfo.getHeadImg());
        userIndexVO.setBindStatus(userInfo.getBindStatus());
        userIndexVO.setAmount(userAccount.getAmount());
        userIndexVO.setFreezeAmount(userAccount.getFreezeAmount());
        userIndexVO.setLastLoginTime(userLoginRecord.getCreateTime());

        return userIndexVO;
    }

    /**
     * 通过bind_code查询手机号
     * @param bindCode
     * @return
     */
    @Override
    public String getMobileByBindCode(String bindCode) {
        QueryWrapper<UserInfo> userInfoQueryWrapper = new QueryWrapper<>();
        userInfoQueryWrapper.eq("bind_code", bindCode);
        UserInfo userInfo = baseMapper.selectOne(userInfoQueryWrapper);
        return userInfo.getMobile();
    }
}
