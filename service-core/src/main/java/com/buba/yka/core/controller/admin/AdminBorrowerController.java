package com.buba.yka.core.controller.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.buba.yka.common.result.R;
import com.buba.yka.core.pojo.entity.Borrower;
import com.buba.yka.core.pojo.vo.BorrowerApprovalVO;
import com.buba.yka.core.pojo.vo.BorrowerDetailVO;
import com.buba.yka.core.service.BorrowerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Api(tags = "借款人管理")
@RestController
@RequestMapping("/admin/core/borrower")
@Slf4j
public class AdminBorrowerController {

    @Resource
    private BorrowerService borrowerService;


    @ApiOperation("后台管理系统获取借款人分页列表")
    @GetMapping("/list/{page}/{limit}")
    public R listPage(
            @ApiParam(value = "当前页码", required = true)
            @PathVariable Long page,

            @ApiParam(value = "每页记录数", required = true)
            @PathVariable Long limit,

            @ApiParam(value = "查询关键字", required = false)
            @RequestParam String keyword) {
        //这里的@RequestParam其实是可以省略的，但是在目前的swagger版本中（2.9.2）不能省略，
        //否则默认将没有注解的参数解析为body中的传递的数据

        Page<Borrower> pageParam = new Page<>(page, limit);
        Page<Borrower> pageModel = borrowerService.listPage(pageParam, keyword);
        return R.ok().data("pageModel", pageModel);
    }


    @ApiOperation("后台管理系统获取借款人的详细信息")
    @GetMapping("/show/{id}")
    public R show(
            @ApiParam(value = "借款人id", required = true)
            @PathVariable Long id) {

        BorrowerDetailVO borrowerDetailVO = borrowerService.getBorrowerDetailVOById(id);

        return R.ok().data("borrowerDetailVO", borrowerDetailVO);
    }


    /**
     * 后台管理系统，借款人信息审批，对借款人信息是否无误进行审核（0：未认证，1：认证中， 2：认证通过， -1：认证失败）和记录积分（在通过的前提下）
     * 对借款人信息是否无误进行审核的目标：
     * (1)在user_integral表中添加积分明细
     * (2)在user_info表中添加总积分（user_info表中的原始积分 + user_integral表中的积分明细之和 ）
     * (3)修改borrower表的借款申请审核状态
     * (4)修改user_info表中的借款申请审核状态
     * @param borrowerApprovalVO 对借款人信息审批认证状态，和积分记录
     * @return
     */
    @ApiOperation("后台管理系统借款人信息审批")
    @PostMapping("/approval")
    public R approval(@RequestBody BorrowerApprovalVO borrowerApprovalVO) {
        borrowerService.approval(borrowerApprovalVO);
        return R.ok().message("审批完成");
    }
}