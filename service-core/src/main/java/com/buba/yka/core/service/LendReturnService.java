package com.buba.yka.core.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.buba.yka.core.pojo.entity.Lend;
import com.buba.yka.core.pojo.entity.LendReturn;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 还款记录表 服务类
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
public interface LendReturnService extends IService<LendReturn> {

    //管理端在标的详情展示还款计划
    List<LendReturn> selectByLendId(Long lendId);

    //网站端在标的详情展示还款计划
    List<LendReturn> selectByLendId(Long lendId,Long userId);

    //对选中的这个期进行还款，尚融宝对汇付宝发送请求并携带参数
    String commitReturn(Long lendReturnId, Long userId);

    //汇付宝向尚融宝发送异步回调请求，并携带固定参数
    void notify(Map<String, Object> paramMap);

    //在网站端当前用户主页面，可查看的还款计划
    Page<LendReturn> selectLendReturn(Page<LendReturn> page1, Long userId);
}
