package com.buba.yka.core.mapper;

import com.buba.yka.core.pojo.entity.UserIntegral;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户积分记录表 Mapper 接口
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
public interface UserIntegralMapper extends BaseMapper<UserIntegral> {

}
