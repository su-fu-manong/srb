package com.buba.yka.core.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.buba.yka.core.pojo.entity.BorrowInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.buba.yka.core.pojo.vo.BorrowInfoApprovalVO;

import java.math.BigDecimal;
import java.util.Map;

/**
 * <p>
 * 借款信息表 服务类
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
public interface BorrowInfoService extends IService<BorrowInfo> {

    //用户界面业务：通过当前用户所获积分评估借款额度
    BigDecimal getBorrowAmount(Long userId);

    //用户界面业务：提交借款申请，新增borrow_info表
    void saveBorrowInfo(BorrowInfo borrowInfo, Long userId);

    //用户界面业务：获取当前登录的借款人的借款申请审批状态
    Integer getStatusByUserId(Long userId);

    //后台管理系统业务：借款申请信息表（borrow_info）进行分页
    Page<BorrowInfo> selectListPage(Page<BorrowInfo> pageParam,String keyword);

    //在网站端当前用户主页面，可查看的借款列表
    Page<BorrowInfo> selectListPage(Page<BorrowInfo> pageParam,Long userId);

    //后台管理系统业务：通过借款申请id查询借款详细信息
    Map<String, Object> getBorrowInfoDetail(Long id);

    //后台管理系统业务：借款申请进行审批
    void approval(BorrowInfoApprovalVO borrowInfoApprovalVO);
}
