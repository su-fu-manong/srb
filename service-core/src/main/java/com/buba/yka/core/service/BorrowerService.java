package com.buba.yka.core.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.buba.yka.core.pojo.entity.Borrower;
import com.baomidou.mybatisplus.extension.service.IService;
import com.buba.yka.core.pojo.vo.BorrowerApprovalVO;
import com.buba.yka.core.pojo.vo.BorrowerDetailVO;
import com.buba.yka.core.pojo.vo.BorrowerVO;

import java.io.Serializable;

/**
 * <p>
 * 借款人 服务类
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
public interface BorrowerService extends IService<Borrower> {

    //用户界面：提交借款人信息，保存借款人表信息borrower，保存借款人附件表信息borrower_attach
    void saveBorrowerVOByUserId(BorrowerVO borrowerVO, Long userId);

    //用户界面：获取借款人认证状态
    Integer getStatusByUserId(Long userId);

    //后台管理系统获取借款人分页列表
    Page<Borrower> listPage(Page<Borrower> pageParam, String keyword);


    //后台管理系统：获取借款人详细信息
    BorrowerDetailVO getBorrowerDetailVOById(Long id);

    //后台管理系统借款额度审批，对借款人借款进行借款审核（0：未认证，1：认证中， 2：认证通过， -1：认证失败）和记录积分（在通过的前提下）
    void approval(BorrowerApprovalVO borrowerApprovalVO);
}
