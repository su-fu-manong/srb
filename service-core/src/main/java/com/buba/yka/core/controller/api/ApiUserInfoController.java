package com.buba.yka.core.controller.api;


import com.buba.yka.base.utils.JwtUtils;
import com.buba.yka.common.exception.Assert;
import com.buba.yka.common.result.R;
import com.buba.yka.common.result.ResponseEnum;
import com.buba.yka.common.utils.RegexValidateUtils;
import com.buba.yka.core.pojo.vo.LoginVO;
import com.buba.yka.core.pojo.vo.RegisterVO;
import com.buba.yka.core.pojo.vo.UserIndexVO;
import com.buba.yka.core.pojo.vo.UserInfoVO;
import com.buba.yka.core.service.UserInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 用户基本信息 前端控制器
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
@Api(tags = "会员用户接口")
@RestController
@RequestMapping("/api/core/userInfo")
@Slf4j
//@CrossOrigin
public class ApiUserInfoController {

    @Resource
    private UserInfoService userInfoService;

    @Resource
    private RedisTemplate redisTemplate;

    @ApiOperation("会员用户注册")
    @PostMapping("/register")
    public R register(@RequestBody RegisterVO registerVO){

        String mobile = registerVO.getMobile();
        String password = registerVO.getPassword();
        String code = registerVO.getCode();

        //MOBILE_NULL_ERROR(-202, "手机号不能为空"),
        Assert.notEmpty(mobile, ResponseEnum.MOBILE_NULL_ERROR);
        //MOBILE_ERROR(-203, "手机号不正确"),
        Assert.isTrue(RegexValidateUtils.checkCellphone(mobile), ResponseEnum.MOBILE_ERROR);
        //MOBILE_ERROR(-203, "密码格式不正确"),
        Assert.isTrue(RegexValidateUtils.checkPassword(password), ResponseEnum.PASSWORD_ERROR);
        //PASSWORD_NULL_ERROR(-204, "密码不能为空"),
        Assert.notEmpty(password, ResponseEnum.PASSWORD_NULL_ERROR);
        //CODE_NULL_ERROR(-205, "验证码不能为空"),
        Assert.notEmpty(code, ResponseEnum.CODE_NULL_ERROR);

        //校验验证码
        String codeGen = (String)redisTemplate.opsForValue().get("srb:sms:code:" + mobile);
        //如果redis中的验证码和前端传过来的验证码不一致，则抛出异常CODE_ERROR(-206, "验证码不正确"),
        Assert.equals(code, codeGen, ResponseEnum.CODE_ERROR);

        //注册
        userInfoService.register(registerVO);
        return R.ok().message("注册成功");
    }


    /**
     * 用户登录，校验登录，新增记录登录日志user_login_record，每登录一次都会记录一次日志、生成token，组织UserInfoVO返回前端
     * @param loginVO 用户登录的消息
     * @param request 需要获取远程主机客户端IP地址（127.0.0.1），记录登录日志表user_login_record填写IP
     * @return
     */
    @ApiOperation("会员用户登录")
    @PostMapping("/login")
    public R login(@RequestBody LoginVO loginVO, HttpServletRequest request) {

        String mobile = loginVO.getMobile();//手机号

        String password = loginVO.getPassword();//密码

        //model为空抛出异常，MOBILE_NULL_ERROR(-202, "手机号码不能为空"),
        Assert.notEmpty(mobile, ResponseEnum.MOBILE_NULL_ERROR);

        //password为空抛出异常，MOBILE_NULL_ERROR(-202, "密码不能为空"),
        Assert.notEmpty(password, ResponseEnum.PASSWORD_NULL_ERROR);

        //远程主机客户端IP地址，记录登录日志表user_login_record需要IP，
        String ip = request.getRemoteAddr();
        UserInfoVO userInfoVO = userInfoService.login(loginVO, ip);

        //返回给前端，存到cookie中
        return R.ok().data("userInfo", userInfoVO);
    }


    /**
     * 访问哪一个请求页面都会校验令牌，公共头部页面srb-site\components\AppHeader.vue这个文件调用了
     * 因为每个网页都有公共头部，跳转每个网页都会来到AppHeader.vue这个页面，来到这个页面就会走mounted初始化调用函数校验token，
     * 登录成功时设置的token，前端把token放cookie存储了，如果没有cookie或token校验不成功，说明没有登录，没有登录成功就不能访问其它请求
     * @param request
     * @return
     */
    @ApiOperation("校验令牌")
    @GetMapping("/checkToken")
    public R checkToken(HttpServletRequest request) {

        String token = request.getHeader("token");
        //校验令牌
        boolean result = JwtUtils.checkToken(token);

        if(result){
            return R.ok();
        }else{
            //LOGIN_AUTH_ERROR(-211, "未登录"),
            return R.setResult(ResponseEnum.LOGIN_AUTH_ERROR);
        }
    }


    /**
     * 被sms服务远程调用了，在发送短信的时候校验手机号是否注册，如果注册过就不能发送短信了
     * @param mobile
     * @return
     */
    @ApiOperation("校验手机号是否注册")
    @GetMapping("/checkMobile/{mobile}")
    public Boolean checkMobile(@PathVariable("mobile") String mobile){

       Boolean result =  userInfoService.checkMobile(mobile);

       return result;

    }


    /**
     * 获取当前用户所有数据，设置到个人中心首页面
     * @param request
     * @return
     */
    @ApiOperation("获取个人空间用户信息")
    @GetMapping("/auth/getIndexUserInfo")
    public R getIndexUserInfo(HttpServletRequest request) {
        String token = request.getHeader("token");
        Long userId = JwtUtils.getUserId(token);
        UserIndexVO userIndexVO = userInfoService.getIndexUserInfo(userId);
        return R.ok().data("userIndexVO", userIndexVO);
    }


}

