package com.buba.yka.core.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.buba.yka.common.exception.Assert;
import com.buba.yka.common.exception.BusinessException;
import com.buba.yka.common.result.ResponseEnum;
import com.buba.yka.core.enums.LendStatusEnum;
import com.buba.yka.core.enums.TransTypeEnum;
import com.buba.yka.core.hfb.FormHelper;
import com.buba.yka.core.hfb.HfbConst;
import com.buba.yka.core.hfb.RequestHelper;
import com.buba.yka.core.mapper.LendItemMapper;
import com.buba.yka.core.mapper.LendMapper;
import com.buba.yka.core.mapper.UserAccountMapper;
import com.buba.yka.core.mapper.UserInfoMapper;
import com.buba.yka.core.pojo.bo.TransFlowBO;
import com.buba.yka.core.pojo.entity.Lend;
import com.buba.yka.core.pojo.entity.LendItem;
import com.buba.yka.core.pojo.entity.UserInfo;
import com.buba.yka.core.pojo.vo.InvestVO;
import com.buba.yka.core.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.buba.yka.core.utils.LendNoUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 标的出借记录表 服务实现类
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
@Service
public class LendItemServiceImpl extends ServiceImpl<LendItemMapper, LendItem> implements LendItemService {

    @Resource
    private LendMapper lendMapper;

    @Resource
    private UserAccountService userAccountService;

    @Resource
    private UserBindService userBindService;

    @Resource
    private LendService lendService;

    @Resource
    private TransFlowService transFlowService;

    @Resource
    private UserAccountMapper userAccountMapper;
    @Resource
    private UserInfoMapper userInfoMapper;

    @Resource
    private DictService dictService;

    /**
     * 投资人对标的进行投资，尚融宝请求汇付宝携带着固定参数（汇付宝要求的）
     * 尚融宝：
     * 新增lend_item表记录（标的出借记录表，记录投资人投资信息的）与汇付宝的user_invest表进行绑定同一个投资编号订单号
     * 汇付宝：
     * 新增user_invest表记录（用户投资表），与尚融宝的lend_item表进行绑定同一个投资编号订单号
     * 更改user_account投资人账户表
     *  通过当前投资人的bind_code更改user_account投资人账户表账户金额和冻结金额字段（投资成功后账户金额-投资金额，冻结金额+投资金额）
     * @param investVO 投资信息对象（标的id，投资金额，投资人id，投资人姓名））
     * @return 给前端返回一个动态html，表单自动提交，生成一个动态表单的字符串，表单携带着固定参数：表单自动提交对汇付宝发送请求
     */
    @Override
    public  String commitInvest(InvestVO investVO) {
//        投标时要在服务器端校验数据：
//        1、标的状态必须为募资中
//        2、标的不能超卖
//        3、账户可用余额充足
//        4、判断当前用户是否绑定账号（汇付包）
//      健壮性校验==========================================
        //获取标的信息
        Long lendId = investVO.getLendId();
        Lend lend = lendMapper.selectById(lendId);
        //1、标的状态必须为募资中,LEND_INVEST_ERROR(305, "当前状态无法投标"),
        Assert.isTrue(lend.getStatus().intValue() == LendStatusEnum.INVEST_RUN.getStatus().intValue(),
                ResponseEnum.LEND_INVEST_ERROR);
        //2、标的不能超卖，已投金额 + 本次投资金额 <= 标的金额（借款金额）如果大于则超卖,LEND_FULL_SCALE_ERROR(306, "已满标，无法投标"),
        BigDecimal sum = lend.getInvestAmount().add(new BigDecimal(investVO.getInvestAmount()));
        Assert.isTrue(sum.doubleValue() <= lend.getAmount().doubleValue()
                ,ResponseEnum.LEND_FULL_SCALE_ERROR);
        //3、判断账户可用余额是否充足 本次投资金额 <= 当前投资人账户余额，如果大于账户余额，则NOT_SUFFICIENT_FUNDS_ERROR(307, "余额不足，请充值"),
        //查询当前投资人账户余额
        BigDecimal account = userAccountService.getAccount(investVO.getInvestUserId());
        Assert.isTrue(Double.parseDouble(investVO.getInvestAmount()) <= account.doubleValue(),
                ResponseEnum.NOT_SUFFICIENT_FUNDS_ERROR);

        //4、判断当前用户是否绑定账号（汇付包）
        //通过用户id获取投资人的绑定协议号
        String bindCode = userBindService.getBindCodeByUserId(investVO.getInvestUserId());
        //通过用户id获取借款人的绑定协议号
        String benefitBindCode = userBindService.getBindCodeByUserId(lend.getUserId());
        if(bindCode==null || benefitBindCode == null){
            //USER_NO_BIND_ERROR(302, "用户未绑定"),
            throw new BusinessException(ResponseEnum.USER_NO_BIND_ERROR);
        }

        //在商户平台（尚融宝）中生成投资信息(lend_item表)==========================================
        //标的下的投资信息
        LendItem lendItem = new LendItem();
        String lendItemNo = LendNoUtils.getLendItemNo();
        lendItem.setLendItemNo(lendItemNo);//设置投资编号（一个Lend对应一个或多个LendItem）
        lendItem.setLendId(investVO.getLendId());//设置对应的标的id
        lendItem.setInvestUserId(investVO.getInvestUserId());//设置投资人用户id
        lendItem.setInvestName(investVO.getInvestName());//设置投资人用户名称
        lendItem.setInvestAmount(new BigDecimal(investVO.getInvestAmount()));//设置本次投资金额
        lendItem.setLendYearRate(lend.getLendYearRate());//设置年化率
        lendItem.setInvestTime(LocalDateTime.now());//设置投资时间
        lendItem.setLendStartDate(lend.getLendStartDate());//设置开始时间
        lendItem.setLendEndDate(lend.getLendEndDate());//设置结束时间
        //计算投资人预期所获收益 本次投资金额 * 年化率/12 * 期数
        BigDecimal expectAmount = lendService.getInterestCount(
                lendItem.getInvestAmount(),
                lend.getLendYearRate(),
                lend.getPeriod(),
                lend.getReturnMethod()
        );
        //设置投资人预期所获收益
        lendItem.setExpectAmount(expectAmount);
        lendItem.setRealAmount(new BigDecimal(0));//设置实际收益
        lendItem.setStatus(0);//设置状态，默认状态：（0：默认 1：已支付 2：已还款）
        baseMapper.insert(lendItem);

        //组装投资相关的参数，提交到汇付宝资金托管平台==========================================
        //在托管平台同步用户的投资信息，修改用户的账户资金信息==========================================
        //封装提交至汇付宝的参数
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("agentId", HfbConst.AGENT_ID);//给商户分配的唯一标识
        paramMap.put("voteBindCode", bindCode);//投资人绑定协议号
        paramMap.put("benefitBindCode",benefitBindCode);//借款人绑定协议号。同一个项目只能有一个借款人。
        paramMap.put("agentProjectCode", lend.getLendNo());//项目(当前标的)编号。只能由数字、字母组成。
        paramMap.put("agentProjectName", lend.getTitle());//项目(当前标的)名称。

        //在资金托管平台上的投资订单的唯一编号，要和lendItemNo保持一致。
        paramMap.put("agentBillNo", lendItemNo);//投资订单编号流水号
        paramMap.put("voteAmt", investVO.getInvestAmount());//本次投资金额。
        paramMap.put("votePrizeAmt", "0");//投资奖励金额。
        paramMap.put("voteFeeAmt", "0");//商户手续费。
        paramMap.put("projectAmt", lend.getAmount()); //标的(借款)总金额
        paramMap.put("note", investVO.getInvestName()+"投资");//投资备注。
        paramMap.put("notifyUrl", HfbConst.INVEST_NOTIFY_URL);///异步通知商户投资成功的完整地址（汇付宝对尚融宝发送请求，携带固定的参数）
        paramMap.put("returnUrl", HfbConst.INVEST_RETURN_URL);//投资完成后同步返回商户的完整地址。返回尚融宝页面的路径
        paramMap.put("timestamp", RequestHelper.getTimestamp());//时间戳
        String sign = RequestHelper.getSign(paramMap);
        paramMap.put("sign", sign);//获取加密后的签名，有固定的格式加密，可以进去看看


        //构建充值自动提交表单，用表单形式提交到汇付包，参数是 paramMap map集合 表单带有自动提交
        // HfbConst.INVEST_URL：提交地址，访问汇付包的接口
        //paramMap：携带的参数
        //formStr：生成了一个动态的表单
        String formStr = FormHelper.buildForm(HfbConst.INVEST_URL, paramMap);


        return formStr;
    }


    /**
     * 异步回调，与汇付宝数据同步，汇付宝向尚融宝发送请求，并携带固定参数
     * 目的：
     * 1、修改账户余额(user_account)，从账户余额中减去投资金额，在冻结金额中增加投资金额（投资成功后先把投资的钱冻结，该标的满标后(借款金额攒够时)，冻结解除）
     * 2、修改投资记录的状态为已支付（lend_item表）（0：默认 1：已支付 2：已还款）
     * 3、修改标的记录(lend表)：投资人数(invest_num字段)，已投金额(invest_amount字段)
     * 4、增加交易流水(trans_flow表)
     * @param paramMap 获取汇付宝向尚融宝发送的参数
     * @return 尚融宝对汇付包返回success，返回success汇付包就不会发送重试了（不会报错了）成功必须返回success
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void notify(Map<String, Object> paramMap) {

        //获取投资订单号流水号
        String agentBillNo = (String) paramMap.get("agentBillNo");

        //幂等性判断，解决的问题：当回调重试时，下面页面在重新执行，这样会一直投标，
        //汇付宝向尚融宝发起回调（汇付宝发送异步通知），但是返回结果的时候网络异常，或返回的不是success,如果没有收到正确的响应 "success"，则汇付宝会发起重试
        //注意：是先处理完业务后在return(响应)的，所以第一次业务是执行完成的（下面(1)(2)(3)(4)是执行完成的），在次发起重试，重试的参数是一样的，这样会导致处理了多次业务，所以这样金额和流水会重复增加
        //所以要判断交易流水是否存在，因为交易订单流水号是唯一标识添加了约束Unique
        boolean saveTransFlow = transFlowService.isSaveTransFlow(agentBillNo);
        //如果重试了，则先判断流水号是否存在，如果存在则从业务方法中直接退出，就不会重试了,就不会进行下面了
        if(saveTransFlow){
            log.warn("幂等性返回");
            return;
        }

        String bindCode = (String)paramMap.get("voteBindCode"); //投资人绑定协议号
        String voteAmt = (String)paramMap.get("voteAmt"); //投资金额

        //(1)、修改账户余额(user_account) 从账户余额中减去投资金额，在冻结金额中增加投资金额（投资成功后先把投资的钱冻结，该标的满标后(借款金额攒够时)，冻结解除）
        //（投资成功后账户金额-投资金额，冻结金额+投资金额）new BigDecimal("-"+voteAmt)：字符串拼接后就变成了负数
        userAccountMapper.updateAccount(bindCode,new BigDecimal("-"+voteAmt),new BigDecimal(voteAmt));

        //(2)、修改投资记录表的状态为已支付（lend_item表）（0：默认 1：已支付 2：已还款）
        //通过投资订单号流水号获取投资信息对象(LendItem)
        LambdaQueryWrapper<LendItem> lendItemLambdaQueryWrapper = new LambdaQueryWrapper<>();
        lendItemLambdaQueryWrapper.eq(LendItem::getLendItemNo,agentBillNo);
        LendItem lendItem = baseMapper.selectOne(lendItemLambdaQueryWrapper);
        lendItem.setStatus(1);
        baseMapper.updateById(lendItem);

        //(3)、修改标的记录(lend表)：已投资人数(invest_num字段)，已投金额(invest_amount字段)
        Lend lend = lendMapper.selectById(lendItem.getLendId());//通过投资记录表中的lend_id获取到标的对象
        lend.setInvestNum(lend.getInvestNum()+1);//设置已投资人数，在原有的基础上+1
        lend.setInvestAmount(lend.getInvestAmount().add(lendItem.getInvestAmount()));//在原有的金额上相加投资金额
        lendMapper.updateById(lend);

        //4、增加交易流水(trans_flow表)
        TransFlowBO transFlowBO = new TransFlowBO();
        transFlowBO.setAmount(new BigDecimal(voteAmt));//设置金额
        transFlowBO.setAgentBillNo(agentBillNo);//设置交易单号，流水号
        transFlowBO.setBindCode(bindCode);//设置投资人绑定协议号
        transFlowBO.setTransTypeEnum(TransTypeEnum.INVEST_LOCK);//设置交易类型，INVEST_LOCK(2,"投标锁定"),
        transFlowBO.setMemo("投资项目编号：" + lend.getLendNo() + "，项目名称：" + lend.getTitle());//描述
        transFlowService.saveTransFlow(transFlowBO);

    }


    /**
     * 通过标的id和状态为1已支付的查询lendItem表信息
     * @param lendId 标的id
     * @param status
     * @return
     */
    @Override
    public List<LendItem> selectByLendId(Long lendId, Integer status) {
        LambdaQueryWrapper<LendItem> lendItemLambdaQueryWrapper = new LambdaQueryWrapper<>();
        lendItemLambdaQueryWrapper.eq(LendItem::getLendId,lendId).eq(LendItem::getStatus,status);
        List<LendItem> lendItems = baseMapper.selectList(lendItemLambdaQueryWrapper);
        return lendItems;
    }

    /**
     * 在标的详情展示投资记录
     * @param lendId
     * @return
     */
    @Override
    public List<LendItem> selectByLendId(Long lendId) {
        LambdaQueryWrapper<LendItem> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.eq(LendItem::getLendId, lendId).eq(LendItem::getStatus,1);
        List<LendItem> lendItemList = baseMapper.selectList(queryWrapper);
        return lendItemList;
    }


    /**
     * 在网站端当前用户主页面，可查看的投资列表
     * @param pageParam
     * @param userId
     * @return
     */
    @Override
    public Page<LendItem> selectListPage(Page<LendItem> pageParam, Long userId) {
        LambdaQueryWrapper<LendItem> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.eq(LendItem::getInvestUserId, userId).eq(LendItem::getStatus,1);
        Page<LendItem> lendItemList = baseMapper.selectPage(pageParam,queryWrapper);
        lendItemList.getRecords().forEach(item ->{
            Lend lend = lendMapper.selectById(item.getLendId());
            item.getParam().put("lendName",lend.getTitle());//标的名称
        });
        return lendItemList;
    }
}
