package com.buba.yka.core.controller.api;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.buba.yka.base.utils.JwtUtils;
import com.buba.yka.common.result.R;
import com.buba.yka.core.pojo.entity.BorrowInfo;
import com.buba.yka.core.service.BorrowInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

/**
 * <p>
 * 借款信息表 前端控制器，后台管理人员同意借款人借款了，现在就可以申请借款了
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
@Api(tags = "借款信息")
@RestController
@RequestMapping("/api/core/borrowInfo")
@Slf4j
public class ApiBorrowInfoController {

    @Resource
    private BorrowInfoService borrowInfoService;

    /**
     * 通过当前用户所获积分评估借款额度，通过积分等级表评估
     * @param request
     * @return
     */
    @ApiOperation("获取借款额度")
    @GetMapping("/auth/getBorrowAmount")
    public R getBorrowAmount(HttpServletRequest request) {
        String token = request.getHeader("token");
        Long userId = JwtUtils.getUserId(token);
        BigDecimal borrowAmount = borrowInfoService.getBorrowAmount(userId);
        return R.ok().data("borrowAmount", borrowAmount);
    }


    /**
     * 提交借款申请，新增borrow_info表
     * 1、实现思路
     * 借款人提交借款要 判断借款人账户绑定状态 与 借款人信息审批状态，只有这两个状态都成立才能借款，这两个状态都在会员表中
     * 目标：将借款申请表单中用户填写的数据保存在borrow_info数据库表中
     * @param borrowInfo 表单信息
     * @param request 请求头，当前登录id
     * @return
     */
    @ApiOperation("提交借款申请")
    @PostMapping("/auth/save")
    public R save(@RequestBody BorrowInfo borrowInfo,HttpServletRequest request){
        String token = request.getHeader("token");//获取请求头
        Long userId = JwtUtils.getUserId(token);//获取userid，校验token

        borrowInfoService.saveBorrowInfo(borrowInfo,userId);

        return R.ok().message("提交申请成功");

    }

    /**
     * 获取当前登录的借款人的借款申请审批状态
     * @param request 请求头
     * @return 审批状态
     */
    @ApiOperation("获取借款申请审批状态")
    @GetMapping("/auth/getBorrowInfoStatus")
    public R getBorrowerStatus(HttpServletRequest request){
        String token = request.getHeader("token");
        Long userId = JwtUtils.getUserId(token);
        Integer status = borrowInfoService.getStatusByUserId(userId);
        return R.ok().data("borrowInfoStatus", status);
    }


    /**
     * 在网站端当前用户主页面，可查看的借款列表
     * @param page
     * @param limit
     * @return
     */
    @ApiOperation("借款申请信息列表分页")
    @GetMapping("/auth/list/{page}/{limit}")
    public R listPage(
            @ApiParam(value = "当前页码", required = true)
            @PathVariable Long page,

            @ApiParam(value = "每页记录数", required = true)
            @PathVariable Long limit,
            HttpServletRequest request
    ) {
        String token = request.getHeader("token");
        Long userId = JwtUtils.getUserId(token);
        Page<BorrowInfo> pageParam = new Page<>(page, limit);

        Page<BorrowInfo> borrowInfoListPage = borrowInfoService.selectListPage(pageParam,userId);

        return R.ok().data("borrowInfoListPage", borrowInfoListPage);
    }

}

