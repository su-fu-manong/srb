package com.buba.yka.core.service;

import com.buba.yka.core.pojo.entity.UserIntegral;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户积分记录表 服务类
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
public interface UserIntegralService extends IService<UserIntegral> {

}
