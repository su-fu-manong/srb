package com.buba.yka.core.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.buba.yka.core.pojo.entity.BorrowInfo;
import com.buba.yka.core.pojo.entity.Lend;
import com.baomidou.mybatisplus.extension.service.IService;
import com.buba.yka.core.pojo.vo.BorrowInfoApprovalVO;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 标的准备表 服务类
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
public interface LendService extends IService<Lend> {

    //后台管理系统：如果借款申请审核通过了，则创建标的（lend）
    void createLend(BorrowInfoApprovalVO borrowInfoApprovalVO, BorrowInfo borrowInfo);

    //后台管理系统：标的列表分页查询
    Page<Lend> selectList(Page<Lend> page1);

    //后台管理系统：获取标的详细信息
    Map<String, Object> getLendDetail(Long id);

    //计算计算投资收益
    BigDecimal getInterestCount(BigDecimal invest, BigDecimal yearRate, Integer totalmonth, Integer returnMethod);

    //放款
    void makeLoan(Long id);
}
