package com.buba.yka.core.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.buba.yka.core.pojo.entity.UserInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.buba.yka.core.pojo.query.UserInfoQuery;
import com.buba.yka.core.pojo.vo.LoginVO;
import com.buba.yka.core.pojo.vo.RegisterVO;
import com.buba.yka.core.pojo.vo.UserIndexVO;
import com.buba.yka.core.pojo.vo.UserInfoVO;

/**
 * <p>
 * 用户基本信息 服务类
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
public interface UserInfoService extends IService<UserInfo> {

    //会员注册，新增用户基本信息，用户账户消息
    void register(RegisterVO registerVO);

    //用户登录
    UserInfoVO login(LoginVO loginVO, String ip);

    //分页加筛选查询
    Page<UserInfo> listPage(Page<UserInfo> pageParam, UserInfoQuery userInfoQuery);

    //更改用户状态status，锁定或解锁
    void lock(Long id, Integer status);

    //校验手机号是否注册
    Boolean checkMobile(String mobile);

    //获取当前用户信息
    UserIndexVO getIndexUserInfo(Long userId);

    //通过bind_code查询手机号
    String getMobileByBindCode(String bindCode);
}
