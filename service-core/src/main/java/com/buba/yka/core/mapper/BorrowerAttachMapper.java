package com.buba.yka.core.mapper;

import com.buba.yka.core.pojo.entity.BorrowerAttach;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 借款人上传资源表 Mapper 接口
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
public interface BorrowerAttachMapper extends BaseMapper<BorrowerAttach> {

}
