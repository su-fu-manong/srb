package com.buba.yka.core.mapper;

import com.buba.yka.core.pojo.entity.Borrower;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 借款人 Mapper 接口
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
public interface BorrowerMapper extends BaseMapper<Borrower> {

}
