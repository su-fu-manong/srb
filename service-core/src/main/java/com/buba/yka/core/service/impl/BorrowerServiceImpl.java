package com.buba.yka.core.service.impl;

import com.alibaba.excel.util.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.buba.yka.core.enums.BorrowerStatusEnum;
import com.buba.yka.core.enums.IntegralEnum;
import com.buba.yka.core.mapper.BorrowerAttachMapper;
import com.buba.yka.core.mapper.BorrowerMapper;
import com.buba.yka.core.mapper.UserInfoMapper;
import com.buba.yka.core.mapper.UserIntegralMapper;
import com.buba.yka.core.pojo.entity.Borrower;
import com.buba.yka.core.pojo.entity.BorrowerAttach;
import com.buba.yka.core.pojo.entity.UserInfo;
import com.buba.yka.core.pojo.entity.UserIntegral;
import com.buba.yka.core.pojo.vo.BorrowerApprovalVO;
import com.buba.yka.core.pojo.vo.BorrowerAttachVO;
import com.buba.yka.core.pojo.vo.BorrowerDetailVO;
import com.buba.yka.core.pojo.vo.BorrowerVO;
import com.buba.yka.core.service.BorrowerAttachService;
import com.buba.yka.core.service.BorrowerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.buba.yka.core.service.DictService;
import com.buba.yka.core.service.UserInfoService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 借款人 服务实现类
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
@Service
public class BorrowerServiceImpl extends ServiceImpl<BorrowerMapper, Borrower> implements BorrowerService {

    @Resource
    private UserInfoMapper userInfoMapper;

    @Resource
    private BorrowerAttachMapper borrowerAttachMapper;

    @Resource
    private DictService dictService;

    @Resource
    private BorrowerAttachService borrowerAttachService;

    @Resource
    private UserIntegralMapper userIntegralMapper;

    /**
     * 用户界面：提交借款人信息，保存借款人表信息borrower，保存借款人附件表信息borrower_attach,更新会员用户userInfo表状态，更新状态为认证中
     * @param borrowerVO 组装的表单数据，借款人输入的信息
     * @param userId 获取token,当前登录用户id
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveBorrowerVOByUserId(BorrowerVO borrowerVO, Long userId) {

        //获取当前用户的基本信息
        UserInfo userInfo = userInfoMapper.selectById(userId);

        //保存借款人表信息borrower
        Borrower borrower = new Borrower();
        BeanUtils.copyProperties(borrowerVO, borrower);//拷贝对象
        borrower.setUserId(userId);//设置用户id
        borrower.setName(userInfo.getName());//设置用户名称
        borrower.setIdCard(userInfo.getIdCard());//设置用户身份证
        borrower.setMobile(userInfo.getMobile());//设置用户电话
        borrower.setStatus(BorrowerStatusEnum.AUTH_RUN.getStatus());//设置状态：认证中
        baseMapper.insert(borrower);

        //保存借款人附件表信息borrower_attach
        List<BorrowerAttach> borrowerAttachList = borrowerVO.getBorrowerAttachList();
        borrowerAttachList.forEach(borrowerAttach -> {
            borrowerAttach.setBorrowerId(borrower.getId());//设置借款人id
            borrowerAttachMapper.insert(borrowerAttach);
        });

        //更新会员用户userInfo表状态，更新状态为认证中
        userInfo.setBorrowAuthStatus(BorrowerStatusEnum.AUTH_RUN.getStatus());
        userInfoMapper.updateById(userInfo);
    }


    /**
     * 用户界面：获取当前登录的账号的借款人信息的审核状态（0：未认证，1：认证中， 2：认证通过， -1：认证失败）
     * @param userId 当前登录的用户id
     * @return 认证状态 （0：未认证，1：认证中， 2：认证通过， -1：认证失败）
     */
    @Override
    public Integer getStatusByUserId(Long userId) {

        //获取当前登录的借款人用户的认证状态
        LambdaQueryWrapper<Borrower> borrowerLambdaQueryWrapper = new LambdaQueryWrapper<>();
        borrowerLambdaQueryWrapper.select(Borrower::getStatus)
                .eq(Borrower::getUserId,userId);

        List<Object> objects = baseMapper.selectObjs(borrowerLambdaQueryWrapper);
        if(objects.size()==0){
            return BorrowerStatusEnum.NO_AUTH.getStatus();
        }

        Integer status = (Integer) objects.get(0);


        return status;
    }


    /**
     * 后台管理系统获取借款人分页列表
     * @param pageParam 页码
     * @param keyword 过滤条件查询
     * @return
     */
    @Override
    public Page<Borrower> listPage(Page<Borrower> pageParam, String keyword) {
        LambdaQueryWrapper<Borrower> borrowerQueryWrapper = new LambdaQueryWrapper<>();

        //如果筛选条件为空则查询全部
        if(StringUtils.isEmpty(keyword)){
            return baseMapper.selectPage(pageParam, null);
        }

        //否则通过筛选条件来模糊查询
        borrowerQueryWrapper.like(Borrower::getName, keyword)
                .or().like(Borrower::getIdCard, keyword)
                .or().like(Borrower::getMobile, keyword)
                .orderByDesc(Borrower::getId);

        return baseMapper.selectPage(pageParam, borrowerQueryWrapper);
    }


    /**
     * 获取借款人的详细信息
     * @param id 借款人id
     * @return
     */
    @Override
    public BorrowerDetailVO getBorrowerDetailVOById(Long id) {

        //通过借款人id获取借款人基本信息
        Borrower borrower = baseMapper.selectById(id);

        //填充基本借款人信息，把能拷贝的都拷贝上，把数据类型和属性名相同的都能拷贝上
        BorrowerDetailVO borrowerDetailVO = new BorrowerDetailVO();
        BeanUtils.copyProperties(borrower,borrowerDetailVO);//对象拷贝，把数据类型和属性名相同的都拷贝上。

        //以下是不能拷贝的，因为数据类型不同
        //1、性别
        borrowerDetailVO.setSex(borrower.getSex()==1?"男":"女");
        //2、婚姻
        borrowerDetailVO.setMarry(borrower.getMarry()?"是":"否");

        //计算下拉列表选中内容,设置下拉列表选中内容
        //因为borrower借款人表中industry、income、return_source、education、relation字段存储的值都是它们对应的子节点的value，都是dict数据字典表中的对应的value字段，都是整数类型
        //所以要通过dict表中的value字段查询出对应的名称，设置在borrowerDetailVO对象中
        borrowerDetailVO.setIndustry(dictService.getNameByParentDictCodeAndValue("industry",borrower.getIndustry()));//行业
        borrowerDetailVO.setEducation(dictService.getNameByParentDictCodeAndValue("education",borrower.getEducation()));//学历
        borrowerDetailVO.setIncome(dictService.getNameByParentDictCodeAndValue("income",borrower.getIncome()));//收入
        borrowerDetailVO.setReturnSource(dictService.getNameByParentDictCodeAndValue("returnSource",borrower.getReturnSource()));//收入来源
        borrowerDetailVO.setContactsRelation(dictService.getNameByParentDictCodeAndValue("relation",borrower.getContactsRelation()));//关系

        //审批状态，设置认证状态（0：未认证，1：认证中， 2：认证通过， -1：认证失败）
        //通过状态号查询出对应的名称
        borrowerDetailVO.setStatus(BorrowerStatusEnum.getMsgByStatus(borrower.getStatus()));

        //获取附件VO列表，通过借款人id查询借款人附件图片
        List<BorrowerAttachVO> borrowerAttachVOList =  borrowerAttachService.selectBorrowerAttachVOList(id);
        borrowerDetailVO.setBorrowerAttachVOList(borrowerAttachVOList);

        return borrowerDetailVO;
    }


    /**
     * 后台管理系统，借款人的信息审批，对借款人信息是否无误进行审核（0：未认证，1：认证中， 2：认证通过， -1：认证失败）和记录积分（在通过的前提下）
     * 对借款人信息是否无误进行审核的目标：
     * (1)在user_integral表中添加积分明细
     * (2)在user_info表中添加总积分（user_info表中的原始积分 + user_integral表中的积分明细之和 ）
     * (3)修改borrower表的借款申请审核状态
     * (4)修改user_info表中的借款申请审核状态
     * @param borrowerApprovalVO 对借款人信息审批认证状态，和积分记录
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void approval(BorrowerApprovalVO borrowerApprovalVO) {

        //获取借款额度申请id（借款人id）
        Long borrowerId = borrowerApprovalVO.getBorrowerId();

        //获取借款额度申请对象（获取借款人对象）
        Borrower borrower = baseMapper.selectById(borrowerId);

        //获取额度申请的审批认证状态（0：未认证，1：认证中， 2：认证通过， -1：认证失败）
        Integer status = borrowerApprovalVO.getStatus();

        //（3）修改borrower表的借款申请审核状态
        borrower.setStatus(status);//设置审批状态
        baseMapper.updateById(borrower);

        //获取用户id
        Long userId = borrower.getUserId();

        //（4）修改user_info表中的借款申请审核状态
        UserInfo userInfo = userInfoMapper.selectById(userId);//通过借款人表用户id，查询用户对象
        userInfo.setBorrowAuthStatus(status);

        //（1）在user_integral表中添加积分明细
        //通过传2，不通过传-1，通过了在加积分
        if(status == 2){
            //1、计算添加基本信息积分
            UserIntegral userIntegral = new UserIntegral();
            userIntegral.setUserId(userId);//设置用户id
            userIntegral.setIntegral(borrowerApprovalVO.getInfoIntegral());//设置积分
            userIntegral.setContent("借款人基本信息积分");//设置获取积分描述说明
            userIntegralMapper.insert(userIntegral);//添加到用户积分记录明细表中

            //2、计算和添加身份证信息积分,判断如果为true就添加积分
            if(borrowerApprovalVO.getIsIdCardOk()){
                userIntegral = new UserIntegral();
                userIntegral.setUserId(userId);//设置用户id
                userIntegral.setIntegral(IntegralEnum.BORROWER_IDCARD.getIntegral());//设置积分
                userIntegral.setContent(IntegralEnum.BORROWER_IDCARD.getMsg());//设置获取积分描述说明
                userIntegralMapper.insert(userIntegral);//添加到用户积分记录明细表中
            }

            //3、计算和添加房产信息积分,判断如果为true就添加积分
            if(borrowerApprovalVO.getIsHouseOk()){
                userIntegral = new UserIntegral();
                userIntegral.setUserId(userId);//设置用户id
                userIntegral.setIntegral(IntegralEnum.BORROWER_HOUSE.getIntegral());//设置积分
                userIntegral.setContent(IntegralEnum.BORROWER_HOUSE.getMsg());//设置获取积分描述说明
                userIntegralMapper.insert(userIntegral);//添加到用户积分记录明细表中
            }

            //4、计算和添加车辆信息积分,判断如果为true就添加积分
            if(borrowerApprovalVO.getIsCarOk()){
                userIntegral = new UserIntegral();
                userIntegral.setUserId(userId);//设置用户id
                userIntegral.setIntegral(IntegralEnum.BORROWER_CAR.getIntegral());//设置积分
                userIntegral.setContent(IntegralEnum.BORROWER_CAR.getMsg());//设置获取积分描述说明
                userIntegralMapper.insert(userIntegral);//添加到用户积分记录明细表中
            }
        }


        //（2）在user_info表中添加总积分（user_info表中的原始积分 + user_integral表中的积分明细之和 ）
        //获取user_info表中的原始积分
        Integer integral = userInfo.getIntegral();

        //获取当前用户的user_integral表中的所有积分明细
        LambdaQueryWrapper<UserIntegral> eq = new LambdaQueryWrapper<UserIntegral>().eq(UserIntegral::getUserId, userId);
        List<UserIntegral> userIntegrals = userIntegralMapper.selectList(eq);

        //遍历集合，user_info表中的原始积分 + 当前用户的user_integral表中的积分明细所有积分
        for (UserIntegral userIntegral1 : userIntegrals) {
            integral+=userIntegral1.getIntegral();
        }

        //在user_info表中设置总积分
        userInfo.setIntegral(integral);

        //修改user_info表中的总积分和审核状态
        userInfoMapper.updateById(userInfo);


    }


}