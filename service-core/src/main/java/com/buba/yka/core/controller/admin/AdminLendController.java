package com.buba.yka.core.controller.admin;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.buba.yka.common.result.R;
import com.buba.yka.core.pojo.entity.Lend;
import com.buba.yka.core.service.LendService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Api(tags = "标的管理")
@RestController
@RequestMapping("/admin/core/lend")
@Slf4j
public class AdminLendController {

    @Resource
    private LendService lendService;

    @ApiOperation("标的列表分页查询")
    @GetMapping("/list/{page}/{limit}")
    public R list(
            @ApiParam(value = "当前页码", required = true)
            @PathVariable Long page,

            @ApiParam(value = "每页记录数", required = true)
            @PathVariable Long limit
    ) {
        Page<Lend> page1 = new Page<>(page,limit);
        Page<Lend> lendList = lendService.selectList(page1);
        return R.ok().data("LendList", lendList);
    }

    /**
     * 后台管理系统：获取标的详细
     * 目的：
     * 1、获取标的详细详细
     * 2、获取借款人详细信息
     * @param id 标的id
     * @return
     */
    @ApiOperation("获取标的详细信息")
    @GetMapping("/show/{id}")
    public R show(
            @ApiParam(value = "标的id", required = true)
            @PathVariable Long id) {
        Map<String, Object> result = lendService.getLendDetail(id);
        return R.ok().data("lendDetail", result);
    }

    /**
     * 后台管理系统：对标的进行放款功能，对汇付宝发送同步远程调用（携带固定参数）
     * 放款是一个同步接口，直接远程调用发送post请求到汇付宝（携带着固定参数），直接响应固定数据(map集合)，没有通过from表单去动态发送请求，也不用异步回调
     * 标的募资时间到，平台会操作放款或撤标，如果达到放款条件则操作放款（并非满标才能放款，可以投资了多少随时放款）
     * step1：放款后汇付宝处理业务：
     * （1）修改user_account表，释放每一个与当前这个标的投标的投资人的冻结金额(freeze_amount)，
     *     把所有对当前标的投资的金额减去平台服务费转账到当前标的的借款人金额(amount)中。（并非满标才能放款，可以投资了多少随时放款）
     *     所以要通过已投金额去计算服务费，那么借款人就收到的金额就是 已投金额-服务费
     * （2）修改user_invest表状态status为1（状态（0：默认 1：已放款 -1：已撤标））
     * step2：汇付宝业务成功后，返回尚融宝放款成功，同步到尚融宝处理业务如下
     * （1）修改标的（lend表）状态（status(2, "还款中")）和 标的平台实际收益（real_amount）和标的放款时间（payment_time）
     * （2）给借款账号转入放款金额（已投金额-服务费）(user_account表) 借款人收到的放款金额就是 已投金额-服务费
     * （3）增加借款交易流水（trans_flow）
     * （4）解冻并扣除投资人资金,释放每一个与当前这个标的投标的投资人的冻结金额(freeze_amount)(user_account表)
     * （5）增加投资人交易流水（trans_flow）
     * （6）生成借款人还款计划（lend_return）和出借人回款计划（lend_item_return）
     * @param id 标的id
     * @return
     */
    @ApiOperation("放款")
    @GetMapping("/makeLoan/{id}")
    public R makeLoan(
            @ApiParam(value = "标的id", required = true)
            @PathVariable("id") Long id) {
        lendService.makeLoan(id);
        return R.ok().message("放款成功");
    }
}