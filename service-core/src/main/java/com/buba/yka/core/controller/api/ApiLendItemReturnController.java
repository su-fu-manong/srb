package com.buba.yka.core.controller.api;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.buba.yka.base.utils.JwtUtils;
import com.buba.yka.common.result.R;
import com.buba.yka.core.pojo.entity.LendItemReturn;
import com.buba.yka.core.pojo.entity.LendReturn;
import com.buba.yka.core.service.LendItemReturnService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Api(tags = "回款计划")
@RestController
@RequestMapping("/api/core/lendItemReturn")
@Slf4j
public class ApiLendItemReturnController {

    @Resource
    private LendItemReturnService lendItemReturnService;

    /**
     * 网站端标的详情中展示回款计划，获取当前标的的所有投资人回款计划，只有当前标的投资人能看见
     * @param lendId 标的id
     * @param request 用户id
     * @return
     */
    @ApiOperation("获取列表")
    @GetMapping("/auth/list/{lendId}")
    public R list(
            @ApiParam(value = "标的id", required = true)
            @PathVariable Long lendId, HttpServletRequest request) {

        String token = request.getHeader("token");
        Long userId = JwtUtils.getUserId(token);

        List<LendItemReturn> list = lendItemReturnService.selectByLendId(lendId, userId);

        if (list.size() == 0) {
            return R.ok().data("list", null);
        }
        return R.ok().data("list", list);
    }


    /**
     * 在网站端当前用户主页面，可查看的回款计划
     * @param request 当前登录用户id
     * @return
     */
    @ApiOperation("获取还款计划列表")
    @GetMapping("/auth/list/{page}/{limit}")
    public R list(
            @ApiParam(value = "当前页码", required = true)
            @PathVariable Long page,

            @ApiParam(value = "每页记录数", required = true)
            @PathVariable Long limit,

            HttpServletRequest request) {

        String token = request.getHeader("token");
        Long userId = JwtUtils.getUserId(token);
        Page<LendItemReturn> page1 = new Page<>(page,limit);
        Page<LendItemReturn> list = lendItemReturnService.selectLendReturnItem(page1,userId);
        return R.ok().data("list", list);
    }
}